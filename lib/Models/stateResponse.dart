// To parse this JSON data, do
//
//     final stateResponse = stateResponseFromJson(jsonString);

import 'dart:convert';

StateResponse stateResponseFromJson(String str) => StateResponse.fromJson(json.decode(str));

String stateResponseToJson(StateResponse data) => json.encode(data.toJson());

class StateResponse {
  StateResponse({
    this.status,
    this.result,
  });

  String status;
  List<Result> result;

  factory StateResponse.fromJson(Map<String, dynamic> json) => StateResponse(
    status: json["status"],
    result: List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "result": List<dynamic>.from(result.map((x) => x.toJson())),
  };
}

class Result {
  Result({
    this.id,
    this.active,
    this.name,
    this.deleted,
    this.districts,
  });

  String id;
  int active;
  String name;
  int deleted;
  List<District> districts;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    id: json["_id"],
    active: json["active"],
    name: json["name"],
    deleted: json["deleted"],
    districts: List<District>.from(json["districts"].map((x) => District.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "active": active,
    "name": name,
    "deleted": deleted,
    "districts": List<dynamic>.from(districts.map((x) => x.toJson())),
  };
}

class District {
  District({
    this.id,
    this.active,
    this.state,
    this.name,
    this.stateName,
    this.deleted,
  });

  String id;
  int active;
  String state;
  String name;
  String stateName;
  int deleted;

  factory District.fromJson(Map<String, dynamic> json) => District(
    id: json["_id"],
    active: json["active"],
    state: json["state"],
    name: json["name"],
    stateName: json["state_name"],
    deleted: json["deleted"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "active": active,
    "state": state,
    "name": name,
    "state_name": stateName,
    "deleted": deleted,
  };
}
