import 'dart:convert';

import 'package:blu_customer/Models/adressResponse.dart';
import 'package:blu_customer/Models/stateResponse.dart';
import 'package:blu_customer/Network/api_call.dart';
import 'package:blu_customer/Network/api_constants.dart';
import 'package:blu_customer/screen/CustomView.dart';
import 'package:blu_customer/screen/Payment.dart';
import 'package:blu_customer/screen/Shipping.dart';
import 'package:blu_customer/screen/confirmation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';

class cheakout extends StatefulWidget {
  final StateResponse stateResponse;
  cheakout({@required this.stateResponse});

  @override
  _cheakoutState createState() => _cheakoutState();
}

class _cheakoutState extends State<cheakout> {
  Future<StateResponse>_future;

  String email;
  String fullName;
  String phoneNo;
  String address;
  String address2;
  String pinCode;
  String state;
  String city;
  String id;
  String response;
  bool isLoading;
  String Messstatus;
  String FullName;
  String label;
  String _myState;
  String _myCity;
  List getCitiesList;
  List stateList;

   Future<String> _getStateList() async {
    //StateResponse stateResponse = StateResponse();
    var response = await http.post(Uri.encodeFull(AppConstants.stateDist),);
    print(response.body);
    if (response.statusCode == 200) {
      var data=json.decode(response.body);
      print(data);
      setState(() {
        stateList=data['result'];
        //getCitiesList = data['result']['districts'];
      });


    }

  }




  List<String> districtList = List();
  List<String> listData = List();
  List<String> idData = List();
  String _mySelection='state';

  final TextEditingController levelController = TextEditingController();
  final TextEditingController address2Controller = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController fullNameController = TextEditingController();
  final TextEditingController addressController = TextEditingController();
  final TextEditingController pinCodeController = TextEditingController();
  final TextEditingController cityController = TextEditingController();
  final TextEditingController stateController = TextEditingController();


  getDataFromSharedPrefs() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      id = sharedPreferences.getString("tokenCall");
      print("welcome to shipping page ravan${id}");
      // firstName = sharedPreferences.getString("firstName");
    });
  }


  final _form = GlobalKey<FormState>();//for storing form state.
  AddressResponse _user;

//saving form after validation
  Future<void> _saveForm() async {
    label=levelController.text;
    fullName =fullNameController.text;
    phoneNo=phoneNumberController.text;
    //email=emailController.text;
    address=addressController.text;
    address2=address2Controller.text;
    state=stateController.text;
    city=cityController.text;
    pinCode=pinCodeController.text;

    final isValid = _form.currentState.validate();
    if (!isValid) {
      return;
    }
    else{
      final AddressResponse addressResponse = await ApiCall.postaddAdress(
        id: id,
        level: label,
        fullname: fullName,
        mobile: phoneNo,
        //email: email,
        address: address,
        pincode: pinCode,
        city: _myState,
        state: _myState,
      );

      if (addressResponse == null) {
        print('data not  sent or error');
      }
      else{
        if (addressResponse.status == "success") {
         // print(addressResponse.status);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => payment()),
          );
          // setState(() {
          //   isLoading = false;
          //   Messstatus = addressResponse.message
          //       .toString();
          //   print("welcvome to upafte profgile ravan${Messstatus}");
          //   response =
          //       addressResponse.status;
          // });
        }

      }
    }
  }
  @override
  void initState() {
    _getStateList();
    // isLoading = false;
    // for (int i = 0; i < widget.stateResponse.result.length; i++) {
    //   listData.add(widget.stateResponse.result[i].name);
    //   idData.add(widget.stateResponse.result[i].id);
    //   print('listData :  $listData');
    // }


    getDataFromSharedPrefs();

    print(id);
    _future=ApiCall.getStateDistList();

    print("Hello ravan your id ${id}");
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar:  AppBar(
        centerTitle: true,
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.pop(context,true);
            }
        ),
        backgroundColor:Colors.indigo[800],
        title: Container(
            padding: const EdgeInsets.only(top: 10,bottom: 5),
            child: Text('Checkout', style: TextStyle(fontSize: 28.0,fontWeight: FontWeight.bold),)),
        actions: <Widget>[
          IconButton(
            iconSize: 35,
            icon: const Icon(Icons.notifications),
            tooltip: 'Show Snackbar',
            onPressed: () {

            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          // color: Colors.red,
          child: Form(
            key: _form,
            child: Column(
              children: [
                Container(

                 // color: Colors.red,
                  width: 350,
                  //height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(



                      child: Text("Shipping",style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.indigo

                      ),),
                      decoration:BoxDecoration(

                          border: Border(
                              bottom: BorderSide(width: 3,color: Colors.green)
                          )
                      ) ,
                     padding: EdgeInsets.all(16),
                    ),
                    Container(
                        child: Text("Payment",style: TextStyle(
                            fontSize: 18,
                            color: Colors.grey
                        ),),
                        decoration:BoxDecoration(
                            border: Border(
                                bottom: BorderSide(width: 3,color: Colors.grey)
                            )
                        ),
                        padding: EdgeInsets.all(16),
                    ),
                    Container(
                        child: Text("Confirmation",style: TextStyle(
                            fontSize: 18,
                            color: Colors.grey
                        ),),
                        decoration:BoxDecoration(
                            border: Border(
                                bottom: BorderSide(width: 3,color: Colors.grey)
                            )
                        ),
                      padding: EdgeInsets.all(16),
                    )
                  ],
                  ),
                ),
                Container(
                  //height: 500,
                  child: Column(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(top: 40),
                        child: Row(
                          children: [
                            Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                    child: Text("First name",style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 18
                                    ),),
                                  ),
                                  Container(
                                    //color: Colors.redAccent,
                                    height: 35,
                                    width: 170,
                                    padding: const EdgeInsets.only(left: 15,right: 10,bottom: 10),
                                    child: TextFormField(
                                      controller: fullNameController,
                                      autocorrect: true,
                                      decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                width: 2,
                                                color: Colors.grey
                                            ),
                                          ),
                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(color:Colors.black)
                                          )
                                      ),

                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(left: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                    child: Text("Last name",style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 18
                                    ),),
                                  ),
                                  Container(
                                    //color: Colors.redAccent,
                                    height: 35,
                                    width: 170,
                                    padding: const EdgeInsets.only(left: 15,right: 10,bottom: 10),
                                    child: TextFormField(
                                      autocorrect: true,
                                      decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                width: 2,
                                                color: Colors.grey
                                            ),
                                          ),
                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(color:Colors.black)
                                          )
                                      ),

                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),

                      //this is for address
                      Container(
                       // color: Colors.redAccent,
                        // margin: const EdgeInsets.only(top: 10,) ,
                        alignment: Alignment.bottomLeft,
                        padding: const EdgeInsets.only(left: 15,top: 15,right: 15,) ,
                        child: Text("Address",style: TextStyle(
                            color: Colors.grey,
                            fontSize: 18
                        ),),
                      ),
                      Container(
                        //alignment: Alignment.bottomLeft,
                       // color: Colors.redAccent,
                        height: 35,
                        width: 345,
                        padding: const EdgeInsets.only(right: 10,bottom: 10),
                        child: TextFormField(
                          controller: addressController,
                          autocorrect: true,
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    width: 2,
                                    color: Colors.grey
                                ),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color:Colors.black)
                              )
                          ),

                        ),
                      ),
                      // second address
                      Container(
                        // margin: const EdgeInsets.only(top: 10,) ,
                        alignment: Alignment.bottomLeft,
                        padding: const EdgeInsets.only(left: 15,top: 15,right: 15,) ,
                        child: Text("Address2",style: TextStyle(
                            color: Colors.grey,
                            fontSize: 18
                        ),),
                      ),
                      Container(
                        alignment: Alignment.bottomLeft,
                        //color: Colors.redAccent,
                        height: 35,
                        width: 345,
                        padding: const EdgeInsets.only(right: 10,bottom: 10),
                        child: TextFormField(
                          controller: address2Controller,
                          autocorrect: true,
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    width: 2,
                                    color: Colors.grey
                                ),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color:Colors.black)
                              )
                          ),

                        ),
                      ),

                      //this is fro lebel
                      Container(
                        // margin: const EdgeInsets.only(top: 10,) ,
                        alignment: Alignment.bottomLeft,
                        padding: const EdgeInsets.only(left: 15,top: 15,right: 15,) ,
                        child: Text("Level",style: TextStyle(
                            color: Colors.grey,
                            fontSize: 18
                        ),),
                      ),
                      Container(
                        alignment: Alignment.bottomLeft,
                        //color: Colors.redAccent,
                        height: 35,
                        width: 345,
                        padding: const EdgeInsets.only(right: 10,bottom: 10),
                        child: TextFormField(
                          controller: levelController,
                          autocorrect: true,
                          decoration: InputDecoration(
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    width: 2,
                                    color: Colors.grey
                                ),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color:Colors.black)
                              )
                          ),

                        ),
                      ),
                      //this is for phone number and zip code
                      Container(
                        child: Row(
                          children: [
                            Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                    child: Text("Phone number",style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 18
                                    ),),
                                  ),
                                  Container(
                                    //color: Colors.redAccent,
                                    height: 35,
                                    width: 170,
                                    padding: const EdgeInsets.only(left: 15,right: 10,bottom: 10),
                                    child: TextFormField(
                                      controller: phoneNumberController,
                                      autocorrect: true,
                                      decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                width: 2,
                                                color: Colors.grey
                                            ),
                                          ),
                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(color:Colors.black)
                                          )
                                      ),

                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: const EdgeInsets.only(left: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                    child: Text("Zip Code",style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 18
                                    ),),
                                  ),
                                  Container(
                                    //color: Colors.redAccent,
                                    height: 35,
                                    width: 170,
                                    padding: const EdgeInsets.only(left: 15,right: 10,bottom: 10),
                                    child: TextFormField(
                                      controller: pinCodeController,
                                      autocorrect: true,
                                      decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                width: 2,
                                                color: Colors.grey
                                            ),
                                          ),
                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(color:Colors.black)
                                          )
                                      ),

                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                      //  width: 300,
                       // color: Colors.red,
                        child: Row(
                         // crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(

                              //color: Colors.red,
                              //width: 150,
                              child: DropdownButtonHideUnderline(
                                child: ButtonTheme(
                                  alignedDropdown: true,
                                  child: DropdownButton<String>(
                                    value: _myState,
                                    iconSize: 28,
                                    icon: (null),
                                    style: TextStyle(
                                      color: Colors.black54,
                                      fontSize: 15
                                    ),
                                    hint: Text('Select State'),
                                    onChanged: (String newValue){
                                      setState(() {
                                        _myState=newValue;
                                        //_getCitiesList();
                                        print('hello ravan how are you${_myState}');
                                      });
                                    },
                                    items: stateList?.map((item){
                                      return new DropdownMenuItem<String>(
                                          child: new Text(item['name']),
                                        value: item['_id'].toString(),
                                      );
                                    })?.toList() ?? [],
                                  ),
                                ),
                              ),
                            ),
                            // SizedBox(
                            //   height: 30,
                            // ),
                            // Container(
                            //   child: Expanded(
                            //       child:DropdownButtonHideUnderline(
                            //         child: ButtonTheme(
                            //           alignedDropdown: true,
                            //           child: DropdownButton<String>(
                            //             value: _myCity,
                            //             iconSize: 28,
                            //             icon: (null),
                            //             style: TextStyle(
                            //                 color: Colors.black54,
                            //                 fontSize: 15
                            //             ),
                            //             hint: Text('Select City'),
                            //             onChanged: (String newValue){
                            //               setState(() {
                            //                 _myCity=newValue;
                            //                 //_getCitiesList();
                            //                 print(_myCity);
                            //               });
                            //             },
                            //             items: getCitiesList?.map((item){
                            //               return new DropdownMenuItem(
                            //                 child: new Text(item['name']),
                            //                 value: item['_id'].toString(),
                            //               );
                            //             })?.toList() ?? [],
                            //           ),
                            //         ),
                            //       )
                            //   ),
                            // ),





                          //   FutureBuilder(
                          // future: _future,
                          // builder: (context, snapshot) {
                          //   if (snapshot.hasData) {
                          //     print("Snapshot has data");
                          //     StateResponse model = snapshot.data;
                          //   for (int i = 0; i < model.result.length; i++) {
                          //     listData.add(model.result[i].name);
                          //     idData.add(model.result[i].id);
                          //     print('list of state :  $listData');
                          //     print('list of stateId :  $idData');
                          //     return DropdownButtonHideUnderline(
                          //       child: new DropdownButton<String>(
                          //        // hint: new Text("Select State"),
                          //         value: _mySelection,
                          //        // isDense: true,
                          //         onChanged: (String newValue) {
                          //           setState(() {
                          //             _mySelection= newValue;
                          //           });
                          //           print(_mySelection);
                          //         },
                          //         items: listData
                          //             .map<DropdownMenuItem<String>>((String value) {
                          //           return new DropdownMenuItem<String>(
                          //             value: value,
                          //             child: new Text(value,
                          //                 style: new TextStyle(color: Colors.black)),
                          //           );
                          //         }).toList(),
                          //
                          //
                          //       ),
                          //     );
                          //
                          //
                          //   }
                          //
                          //   }
                          // }
                          // ),

                            // Container(
                            //   margin: const EdgeInsets.only(left: 10),
                            //   child: Column(
                            //     mainAxisAlignment: MainAxisAlignment.start,
                            //     crossAxisAlignment: CrossAxisAlignment.start,
                            //     children: [
                            //       Container(
                            //         padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                            //         child: Text("City",style: TextStyle(
                            //             color: Colors.grey,
                            //             fontSize: 18
                            //         ),),
                            //       ),
                            //       Container(
                            //         //color: Colors.redAccent,
                            //         height: 35,
                            //         width: 150,
                            //         padding: const EdgeInsets.only(left: 15,right: 10,bottom: 10),
                            //         child: TextFormField(
                            //           controller: cityController,
                            //           autocorrect: true,
                            //           decoration: InputDecoration(
                            //               enabledBorder: UnderlineInputBorder(
                            //                 borderSide: BorderSide(
                            //                     width: 2,
                            //                     color: Colors.grey
                            //                 ),
                            //               ),
                            //               focusedBorder: UnderlineInputBorder(
                            //                   borderSide: BorderSide(color:Colors.black)
                            //               )
                            //           ),
                            //
                            //         ),
                            //       ),
                            //     ],
                            //   ),
                            // ),

                          ],
                        ),
                      ),
                    ],
                  ),
                ),


                //this is for the save button

                Container(

                  alignment: Alignment.center,
                  width: 400,
                  height: 55,
                  margin:  EdgeInsets.only(top:170),
                  padding:  const EdgeInsets.all(10),
                  decoration: BoxDecoration(

                      color: Colors.indigo[800],
                      borderRadius:  BorderRadius.circular(1)
                  ),
                  child: InkWell(
                    onTap: ()=> _saveForm(),


                    child: Text("CONTINUE TO PAYMENT",style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white
                    ),),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}



