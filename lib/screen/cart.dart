import 'package:blu_customer/Models/cartResponse.dart';
import 'package:blu_customer/screen/BottomNavigationBar.dart';
import 'package:blu_customer/screen/Shipping.dart';
import 'package:blu_customer/screen/addressList.dart';
import 'package:blu_customer/screen/checkout.dart';
import 'package:blu_customer/screen/cart_items.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_cart/flutter_cart.dart';
import 'dart:io';

import 'package:provider/provider.dart';

class cart extends StatefulWidget {
  static const routeName ='/cart';
  //  final String id,name,image,price;
  // cart(this.id,this.name,this.image,this.price, {Key key}): super(key: key);
  //cart(String id, String name, String image,String price);




  @override
  _cartState createState() => _cartState();
}

class _cartState extends State<cart> {


 // final cartData=Provider.of<Cart>(context);
  var cart = Cart();
  String id,name,image,price;
  @override
  void initState() {
  // id=widget.id;
  // name=widget.name;


    print("Welcome to add to cart page  ravan your id ${id}");
  print("Welcome to add to cart page  ravan your id ${name}");

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final cart=Provider.of<Cart>(context,);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MyNavigationBar()),
              );
            }
        ),
        backgroundColor:Colors.indigo[900],
        title: Container(
            padding: const EdgeInsets.only(top: 10,bottom: 5),
            child: Text('Cart', style: TextStyle(fontSize: 28.0,fontWeight: FontWeight.bold),)),
      ),
      body: SingleChildScrollView(
        child: Container(
            color: Colors.grey[300],

          child:Column(
            children: [
              Container(
                padding:  const EdgeInsets.all(15),
                color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: const EdgeInsets.only(left: 20),
                      child: Text("PRODUCTS",style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 18
                ),),
                    ),
                    Container(
                      padding: const EdgeInsets.only(right: 30),
                      child: Text(cart.itemCount.toString(),style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 20
                      ),),
                    )
                  ],
                ),
              ),

              Container(
                height: 450,
                color: Colors.grey[300],
                child: Expanded(
                  child: ListView.builder(
                      scrollDirection: Axis.vertical,
                    itemCount: cart.items.length,
                    itemBuilder: (ctx,i) => CartItems(
                        cart.items.values.toList()[i].id,
                        cart.items.keys.toList()[i],
                        cart.items.values.toList()[i].price,
                        cart.items.values.toList()[i].quantity,
                        cart.items.values.toList()[i].name,
                        cart.items.values.toList()[i].image,


                    ),
                    // {
                    //     // return Container(
                    //     //   decoration: BoxDecoration(
                    //     //       color: Colors.grey[300],
                    //     //       border: Border(
                    //     //         bottom: BorderSide(width: 2.0, color: Colors.grey[400]),
                    //     //       )
                    //     //   ),
                    //     //   margin: const EdgeInsets.only(left: 10,right: 5),
                    //     //   padding:  const EdgeInsets.all(13),
                    //     //   child: Row(
                    //     //     mainAxisAlignment: MainAxisAlignment.start,
                    //     //     children: [
                    //     //       Container(
                    //     //         decoration: BoxDecoration(
                    //     //             color: Colors.white,
                    //     //             borderRadius: BorderRadius.circular(7)
                    //     //         ),
                    //     //         padding:  const EdgeInsets.all(15),
                    //     //         child: Image.network('http://139.59.75.40:4040/uploads/product/${cart.items.values.toList()[index].image}',fit: BoxFit.fill,height: 65,width: 65,),
                    //     //       ),
                    //     //       Container(
                    //     //         width:200,
                    //     //
                    //     //         margin: const EdgeInsets.only(left:10),
                    //     //         padding:  const EdgeInsets.all(5),
                    //     //         child: Column(
                    //     //           mainAxisAlignment: MainAxisAlignment.start,
                    //     //           crossAxisAlignment: CrossAxisAlignment.start,
                    //     //           children: [
                    //     //             Container(
                    //     //                 padding:  const EdgeInsets.all(3),
                    //     //                 child: Text(cart.items.values.toList()[index].name,style: TextStyle(
                    //     //                     fontSize: 15,
                    //     //                     fontWeight: FontWeight.bold
                    //     //                 ),)
                    //     //             ),
                    //     //             Container(
                    //     //                 //padding:  const EdgeInsets.all(1),
                    //     //                 //alignment: Alignment.bottomRight,
                    //     //                 child: Text(cart.items.values.toList()[index].name,style: TextStyle(
                    //     //                     color: Colors.grey[700]
                    //     //                 ),)
                    //     //             ),
                    //     //             Container(
                    //     //                 padding:  const EdgeInsets.all(1),
                    //     //                 child: Row(
                    //     //                   children: [
                    //     //                     Container(
                    //     //                       child: Text("\u20B9",style: TextStyle(
                    //     //                           color: Colors.indigo[800]
                    //     //                       ),),
                    //     //                     ),
                    //     //                     Container(
                    //     //
                    //     //                       child: Text(cart.items.values.toList()[index].price.toString(),style: TextStyle(
                    //     //                           color: Colors.indigo[800]
                    //     //                       ),),
                    //     //                     ),
                    //     //
                    //     //                   ],
                    //     //                 )
                    //     //             ),
                    //     //             Container(
                    //     //                 child:Row(
                    //     //                   mainAxisAlignment: MainAxisAlignment.start,
                    //     //                   children: [
                    //     //                     Container(
                    //     //                       alignment: Alignment.center,
                    //     //                       height: 20,
                    //     //                       width: 20,
                    //     //                       color: Colors.green,
                    //     //                       child: Text("-",style: TextStyle(
                    //     //                           color: Colors.white,fontWeight: FontWeight.bold
                    //     //                       ),),
                    //     //                     ),
                    //     //                     Container(
                    //     //                       alignment: Alignment.center,
                    //     //                       height: 20,
                    //     //                       width: 20,
                    //     //                       child: Text(cart.items.values.toList()[index].quantity.toString(),style: TextStyle(
                    //     //                           color: Colors.black
                    //     //                       ),),
                    //     //                     ),
                    //     //                     Container(
                    //     //                       alignment: Alignment.center,
                    //     //                       height: 20,
                    //     //                       width: 20,
                    //     //                       color: Colors.green,
                    //     //                       child: Text("+",style: TextStyle(
                    //     //                           color: Colors.white,fontWeight: FontWeight.bold
                    //     //                       ),),
                    //     //                     ),
                    //     //
                    //     //                   ],
                    //     //                 )
                    //     //             ),
                    //     //           ],
                    //     //         ),
                    //     //
                    //     //       ),
                    //     //       Container(
                    //     //         margin:const EdgeInsets.only(left: 7,bottom: 50) ,
                    //     //         child: Expanded (
                    //     //           child: IconButton(
                    //     //               icon: Icon(Icons.delete),
                    //     //               iconSize: 35,
                    //     //               color: Colors.red,
                    //     //               splashColor: Colors.purple,
                    //     //               onPressed: () {
                    //     //                 Provider.of<Cart>(context).removeItem(cart.items.values.toList()[index].id);
                    //     //               print(cart.items.values.toList()[index].id);
                    //     //             },
                    //     //           ),
                    //     //         )
                    //     //       ),
                    //     //     ],
                    //     //   ),
                    //     //
                    //     // );
                    //
                    // }
                    
                  ),
                ),
              ),

              Container(


                alignment: Alignment.center,
                width: 330,
                 margin: const EdgeInsets.only(bottom: 50),
                  padding:  const EdgeInsets.all(8),
                 decoration: BoxDecoration(
                  color: Colors.indigo[800],
                  borderRadius:  BorderRadius.circular(30)
                ),
                  child: InkWell(
                    onTap: (){
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context)=>addressList(),
                          )
                      );

                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding:  const EdgeInsets.all(10),
                        child: Text("PLACE THIS ORDER :",style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),),
                      ),
                      Container(
                       // margin:const EdgeInsets.only(left: 10),
                       // padding:  const EdgeInsets.all(10),
                        child: Text("\u20B9",style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),),
                      ),
                      Container(
                        margin:const EdgeInsets.only(right: 10),
                        padding:  const EdgeInsets.only(top: 10,bottom: 10,right: 1,left: 2),
                        child: Text(cart.totalAmount.toString(),style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),),
                      ),
                    ],
                ),
                  )
              ),
            ],
          ) ,
        ),
      ),
    );
  }
}


