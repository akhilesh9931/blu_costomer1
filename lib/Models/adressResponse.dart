// To parse this JSON data, do
//
//     final addressResponse = addressResponseFromJson(jsonString);

import 'dart:convert';

AddressResponse addressResponseFromJson(String str) => AddressResponse.fromJson(json.decode(str));

String addressResponseToJson(AddressResponse data) => json.encode(data.toJson());

class AddressResponse {
  AddressResponse({
    this.status,
    this.message,
    this.data,
    this.token,
  });

  String status;
  String message;
  Data data;
  String token;

  factory AddressResponse.fromJson(Map<String, dynamic> json) => AddressResponse(
    status: json["status"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
    token: json["token"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
    "token": token,
  };
}

class Data {
  Data({
    this.deleted,
    this.id,
    this.lebel,
    this.contactName,
    this.contactNumber,
    this.customer,
    this.pincode,
    this.address1,
    this.address2,
    this.state,
    this.district,
    this.createdDate,
    this.v,
  });

  int deleted;
  String id;
  String lebel;
  String contactName;
  String contactNumber;
  String customer;
  String pincode;
  String address1;
  String address2;
  String state;
  String district;
  DateTime createdDate;
  int v;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    deleted: json["deleted"],
    id: json["_id"],
    lebel: json["lebel"],
    contactName: json["contact_name"],
    contactNumber: json["contact_number"],
    customer: json["customer"],
    pincode: json["pincode"],
    address1: json["address_1"],
    address2: json["address_2"],
    state: json["state"],
    district: json["district"],
    createdDate: DateTime.parse(json["created_date"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "deleted": deleted,
    "_id": id,
    "lebel": lebel,
    "contact_name": contactName,
    "contact_number": contactNumber,
    "customer": customer,
    "pincode": pincode,
    "address_1": address1,
    "address_2": address2,
    "state": state,
    "district": district,
    "created_date": createdDate.toIso8601String(),
    "__v": v,
  };
}
