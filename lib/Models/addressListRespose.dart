// To parse this JSON data, do
//
//     final addressListResponse = addressListResponseFromJson(jsonString);

import 'dart:convert';

AddressListResponse addressListResponseFromJson(String str) => AddressListResponse.fromJson(json.decode(str));

String addressListResponseToJson(AddressListResponse data) => json.encode(data.toJson());

class AddressListResponse {
  AddressListResponse({
    this.status,
    this.token,
    this.result,
  });

  String status;
  String token;
  List<Result> result;

  factory AddressListResponse.fromJson(Map<String, dynamic> json) => AddressListResponse(
    status: json["status"],
    token: json["token"],
    result: List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "token": token,
    "result": List<dynamic>.from(result.map((x) => x.toJson())),
  };
}

class Result {
  Result({
    this.deleted,
    this.id,
    this.lebel,
    this.contactName,
    this.contactNumber,
    this.customer,
    this.pincode,
    this.address1,
    this.address2,
    this.state,
    this.district,
    this.createdDate,
    this.v,
  });

  int deleted;
  String id;
  String lebel;
  String contactName;
  String contactNumber;
  Customer customer;
  String pincode;
  String address1;
  String address2;
  String state;
  String district;
  DateTime createdDate;
  int v;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    deleted: json["deleted"],
    id: json["_id"],
    lebel: json["lebel"],
    contactName: json["contact_name"],
    contactNumber: json["contact_number"],
    customer: customerValues.map[json["customer"]],
    pincode: json["pincode"],
    address1: json["address_1"],
    address2: json["address_2"] == null ? null : json["address_2"],
    state: json["state"],
    district: json["district"],
    createdDate: DateTime.parse(json["created_date"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "deleted": deleted,
    "_id": id,
    "lebel": lebel,
    "contact_name": contactName,
    "contact_number": contactNumber,
    "customer": customerValues.reverse[customer],
    "pincode": pincode,
    "address_1": address1,
    "address_2": address2 == null ? null : address2,
    "state": state,
    "district": district,
    "created_date": createdDate.toIso8601String(),
    "__v": v,
  };
}

enum Address1 { BUXAR, PATNA }

final address1Values = EnumValues({
  "buxar": Address1.BUXAR,
  "patna": Address1.PATNA
});

enum Customer { THE_60313_E8_B24_A1_D91_C86_F80_C04 }

final customerValues = EnumValues({
  "60313e8b24a1d91c86f80c04": Customer.THE_60313_E8_B24_A1_D91_C86_F80_C04
});

enum Lebel { HOME, PATNA }

final lebelValues = EnumValues({
  "home": Lebel.HOME,
  "patna": Lebel.PATNA
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
