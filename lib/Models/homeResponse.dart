// To parse this JSON data, do
//
//     final homeResponse = homeResponseFromJson(jsonString);

import 'dart:convert';

HomeResponse homeResponseFromJson(String str) => HomeResponse.fromJson(json.decode(str));

String homeResponseToJson(HomeResponse data) => json.encode(data.toJson());

class HomeResponse {
  HomeResponse({
    this.status,
    this.token,
    this.sliderBaseUrl,
    this.productBaseUrl,
    this.categoryBaseUrl,
    this.result,
  });

  String status;
  String token;
  String sliderBaseUrl;
  String productBaseUrl;
  String categoryBaseUrl;
  Result result;

  factory HomeResponse.fromJson(Map<String, dynamic> json) => HomeResponse(
    status: json["status"],
    token: json["token"],
    sliderBaseUrl: json["slider_base_url"],
    productBaseUrl: json["product_base_url"],
    categoryBaseUrl: json["category_base_url"],
    result: Result.fromJson(json["result"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "token": token,
    "slider_base_url": sliderBaseUrl,
    "product_base_url": productBaseUrl,
    "category_base_url": categoryBaseUrl,
    "result": result.toJson(),
  };
}

class Result {
  Result({
    this.productParentGroup,
    this.products,
    this.sliderImages,
  });

  List<ProductParentGroup> productParentGroup;
  List<Product> products;
  List<ProductParentGroup> sliderImages;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    productParentGroup: List<ProductParentGroup>.from(json["product_parent_group"].map((x) => ProductParentGroup.fromJson(x))),
    products: List<Product>.from(json["products"].map((x) => Product.fromJson(x))),
    sliderImages: List<ProductParentGroup>.from(json["slider_images"].map((x) => ProductParentGroup.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "product_parent_group": List<dynamic>.from(productParentGroup.map((x) => x.toJson())),
    "products": List<dynamic>.from(products.map((x) => x.toJson())),
    "slider_images": List<dynamic>.from(sliderImages.map((x) => x.toJson())),
  };
}

class ProductParentGroup {
  ProductParentGroup({
    this.active,
    this.deleted,
    this.id,
    this.parent,
    this.name,
    this.image,
    this.createdDate,
    this.v,
  });

  int active;
  int deleted;
  String id;
  String parent;
  String name;
  String image;
  DateTime createdDate;
  int v;

  factory ProductParentGroup.fromJson(Map<String, dynamic> json) => ProductParentGroup(
    active: json["active"],
    deleted: json["deleted"],
    id: json["_id"],
    parent: json["parent"] == null ? null : json["parent"],
    name: json["name"],
    image: json["image"],
    createdDate: DateTime.parse(json["created_date"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "active": active,
    "deleted": deleted,
    "_id": id,
    "parent": parent == null ? null : parent,
    "name": name,
    "image": image,
    "created_date": createdDate.toIso8601String(),
    "__v": v,
  };
}

class Product {
  Product({
    this.id,
    this.parentGroup,
    this.childGroup,
    this.brand,
    this.unit,
    this.state,
    this.active,
    this.qty,
    this.deleted,
    this.name,
    this.unitValue,
    this.vendor,
    this.manufacturingPrice,
    this.sellingPriceOrg,
    this.sellingPrice,
    this.cgst,
    this.sgst,
    this.igst,
    this.batchCode,
    this.totalProductPack,
    this.totalPackBox,
    this.boxHeight,
    this.boxLength,
    this.boxWidth,
    this.city,
    this.image,
    this.createdDate,
    this.v,
  });

  String id;
  String parentGroup;
  String childGroup;
  String brand;
  String unit;
  String state;
  int active;
  int qty;
  int deleted;
  String name;
  String unitValue;
  String vendor;
  String manufacturingPrice;
  String sellingPriceOrg;
  String sellingPrice;
  String cgst;
  String sgst;
  String igst;
  String batchCode;
  String totalProductPack;
  String totalPackBox;
  String boxHeight;
  String boxLength;
  String boxWidth;
  String city;
  String image;
  DateTime createdDate;
  int v;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    id: json["_id"],
    parentGroup: json["parent_group"],
    childGroup: json["child_group"],
    brand: json["brand"],
    unit: json["unit"],
    state: json["state"],
    active: json["active"],
    qty: json["qty"],
    deleted: json["deleted"],
    name: json["name"],
    unitValue: json["unit_value"],
    vendor: json["vendor"],
    manufacturingPrice: json["manufacturing_price"],
    sellingPriceOrg: json["selling_price_org"],
    sellingPrice: json["selling_price"],
    cgst: json["cgst"],
    sgst: json["sgst"],
    igst: json["igst"],
    batchCode: json["batch_code"],
    totalProductPack: json["total_product_pack"],
    totalPackBox: json["total_pack_box"],
    boxHeight: json["box_height"],
    boxLength: json["box_length"],
    boxWidth: json["box_width"],
    city: json["city"],
    image: json["image"],
    createdDate: DateTime.parse(json["created_date"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "_id": id,
    "parent_group": parentGroup,
    "child_group": childGroup,
    "brand": brand,
    "unit": unit,
    "state": state,
    "active": active,
    "qty": qty,
    "deleted": deleted,
    "name": name,
    "unit_value": unitValue,
    "vendor": vendor,
    "manufacturing_price": manufacturingPrice,
    "selling_price_org": sellingPriceOrg,
    "selling_price": sellingPrice,
    "cgst": cgst,
    "sgst": sgst,
    "igst": igst,
    "batch_code": batchCode,
    "total_product_pack": totalProductPack,
    "total_pack_box": totalPackBox,
    "box_height": boxHeight,
    "box_length": boxLength,
    "box_width": boxWidth,
    "city": city,
    "image": image,
    "created_date": createdDate.toIso8601String(),
    "__v": v,
  };
}
