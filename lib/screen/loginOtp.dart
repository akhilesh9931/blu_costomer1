import 'package:blu_customer/Models/otpverify.dart';
import 'package:blu_customer/Models/signUpResponse.dart';
import 'package:blu_customer/Network/api_call.dart';
import 'package:blu_customer/screen/BottomNavigationBar.dart';
import 'package:blu_customer/screen/login.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/widgets.dart';
import 'package:blu_customer/screen/signup.dart';

class loginOtp extends StatefulWidget {
  final String otpData;
  final String mobileNo;
  loginOtp(this.otpData, this.mobileNo, {Key key}): super(key: key);

  @override
  _loginOtpState createState() => _loginOtpState();
}

class _loginOtpState extends State<loginOtp> {

  final TextEditingController loginOtpController = TextEditingController();
  final _form = GlobalKey<FormState>();

 String mobile;
  String address;
  String village;
  String postOffice;
  String thana;
  String pincode;
 String name;
 String Mobile;
 String id;
 String otp;
 bool isLoading;
 String email;
 String response;

  Future<void> _VerifyLoginOtp() async {
    final isValid = _form.currentState.validate();
    if (!isValid) {
      return;
    }
    else{
      mobile=widget.mobileNo;
      otp=loginOtpController.text;
      VerifyOtpResponse verifyOtpResponse = await ApiCall.postverifyOtp(
          mobile: mobile,
          otp: otp
      );
      if (verifyOtpResponse == null) {
        print('data not  sent or error');
      }
      else{
        if (verifyOtpResponse.status == "success") {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MyNavigationBar()),
          );
          print(verifyOtpResponse.status);
          print(verifyOtpResponse.message);

          final SharedPreferences sharedPreferences=await SharedPreferences.getInstance();
          sharedPreferences.setString("user_id", verifyOtpResponse.result.id.toString());
          sharedPreferences.setString("user_otp", verifyOtpResponse.result.otp.toString());
          // sharedPreferences.setString("tokenCall", verifyOtpResponse.result.id.toString());
          // sharedPreferences.setString("fullName", verifyOtpResponse.result.fullName.toString());
          // sharedPreferences.setString("mobile", verifyOtpResponse.result.mobile.toString());
          // sharedPreferences.setString("profileImage", verifyOtpResponse.result.profileImage.toString());
          // sharedPreferences.setString("email", verifyOtpResponse.result.email.toString());
          // sharedPreferences.setString("state", verifyOtpResponse.result.state.toString());
          // sharedPreferences.setString("dist", verifyOtpResponse.result.district.toString());
          // sharedPreferences.setString("address", verifyOtpResponse.result.address.toString());
          // sharedPreferences.setString("village", verifyOtpResponse.result.village.toString());
          // sharedPreferences.setString("pincode", verifyOtpResponse.result.pincode.toString());
          // sharedPreferences.setString("thana", verifyOtpResponse.result.thana.toString());
          // sharedPreferences.setString("postOffice", verifyOtpResponse.result.postOffice.toString());
          // sharedPreferences..setBool("isSigned", true);

          // Navigator.push(
          //   context,
          //   MaterialPageRoute(builder: (context) => MyNavigationBar()),
          // );

          setState(() {
            ApiCall.tokenCall= verifyOtpResponse.result.id.toString();



            sharedPreferences.setString("tokenCall", verifyOtpResponse.result.id.toString());
            sharedPreferences.setString("fullName", verifyOtpResponse.result.fullName.toString());
            sharedPreferences.setString("mobile", verifyOtpResponse.result.mobile.toString());
            sharedPreferences.setString("profileImage", verifyOtpResponse.result.profileImage.toString());
            sharedPreferences.setString("email", verifyOtpResponse.result.email.toString());
            sharedPreferences.setString("state", verifyOtpResponse.result.state.toString());
            sharedPreferences.setString("dist", verifyOtpResponse.result.district.toString());
            sharedPreferences.setString("address", verifyOtpResponse.result.address.toString());
            sharedPreferences.setString("village", verifyOtpResponse.result.village.toString());
            sharedPreferences.setString("pincode", verifyOtpResponse.result.pincode.toString());
            sharedPreferences.setString("thana", verifyOtpResponse.result.thana.toString());
            sharedPreferences.setString("postOffice", verifyOtpResponse.result.postOffice.toString());
            sharedPreferences..setBool("isSigned", true);
            print(verifyOtpResponse.result.id.toString());
            isLoading = false;
            id = verifyOtpResponse.result.id;
            Mobile = verifyOtpResponse.result.mobile;
            email = verifyOtpResponse.result.email;
            print(Mobile);
            print(verifyOtpResponse.message);
            print(email);
            print(id);
            response =
                verifyOtpResponse.status;
            // print("signupResponse of it is:"+ signUpResponse)
          });
        }
        else{
          print("error or data not found");
        }
      }
    }
    // else{
    //   mobile=widget.mobileNo;
    //   if (widget.otpData==loginOtpController.text){
    //     Navigator.push(
    //       context,
    //       MaterialPageRoute(builder: (context) => MyNavigationBar()),
    //     );
    //   }
    //   else{
    //     print('otp didnot verify');
    //     print(loginOtpController.text);
    //     print(widget.otpData);
    //     print(widget.mobileNo);
    //   }
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(top: 70),
          color: Colors.white,
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.only(right: 60),
                // color: Colors.blue,
                width: 500,
                height: 200,

                alignment: Alignment.center,
                child: Image.asset("assets/loginlogo.png"),

              ),


              Container(
                  padding: const EdgeInsets.only(top: 10,right: 20,left: 20),

                  //color: Colors.green,
                  width: 500,
                  height: 390,
                  child: Form(
                    key: _form,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          // padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            controller: loginOtpController,
                            autocorrect: true,
                            validator: (text) {
                              if (!(text.length == 6) && text.isNotEmpty) {
                                return "Enter Valid 6 digit OTP No";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: "OTP",
                              prefixIcon: Icon(Icons.lock),
                              hintStyle: TextStyle(color: Colors.grey),
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.black, width: 1),

                              ),
                            ),

                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 10),
                          width: 450,
                          height: 60,
                          //color: Colors.red,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                side: BorderSide(color: Colors.black)),
                            onPressed: () =>  _VerifyLoginOtp(),
                            color: Colors.indigo[800],
                            textColor: Colors.black,
                            child: Text("VERIFY OTP",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold
                              ),),

                          ),

                        ),




                      ],
                    ),
                  )
              ),
              Container(
                //padding: const EdgeInsets.only(top: 70),
                //color: Colors.blue,
                alignment: Alignment.bottomRight,
                // width: 500,
                //height: 300,
                child: Image.asset("assets/wave.png"),
              )

            ],
          ),
        ),
      ),
    );
  }
}
