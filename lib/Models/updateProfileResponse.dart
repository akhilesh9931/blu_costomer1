// To parse this JSON data, do
//
//     final updateProfileResponse = updateProfileResponseFromJson(jsonString);

import 'dart:convert';

UpdateProfileResponse updateProfileResponseFromJson(String str) => UpdateProfileResponse.fromJson(json.decode(str));

String updateProfileResponseToJson(UpdateProfileResponse data) => json.encode(data.toJson());

class UpdateProfileResponse {
  UpdateProfileResponse({
    this.status,
    this.message,
    this.data,
  });

  String status;
  String message;
  Data data;

  factory UpdateProfileResponse.fromJson(Map<String, dynamic> json) => UpdateProfileResponse(
    status: json["status"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.position,
    this.fullName,
    this.address,
    this.village,
    this.postOffice,
    this.thana,
    this.state,
    this.district,
    this.pincode,
    this.mobile,
    this.email,
    this.otp,
    this.profileImage,
    this.deleted,
    this.verified,
    this.id,
    this.createdDate,
    this.v,
  });

  Position position;
  String fullName;
  String address;
  String village;
  String postOffice;
  String thana;
  dynamic state;
  dynamic district;
  String pincode;
  String mobile;
  String email;
  int otp;
  String profileImage;
  int deleted;
  int verified;
  String id;
  DateTime createdDate;
  int v;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    position: Position.fromJson(json["position"]),
    fullName: json["full_name"],
    address: json["address"],
    village: json["village"],
    postOffice: json["post_office"],
    thana: json["thana"],
    state: json["state"],
    district: json["district"],
    pincode: json["pincode"],
    mobile: json["mobile"],
    email: json["email"],
    otp: json["otp"],
    profileImage: json["profile_image"],
    deleted: json["deleted"],
    verified: json["verified"],
    id: json["_id"],
    createdDate: DateTime.parse(json["created_date"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "position": position.toJson(),
    "full_name": fullName,
    "address": address,
    "village": village,
    "post_office": postOffice,
    "thana": thana,
    "state": state,
    "district": district,
    "pincode": pincode,
    "mobile": mobile,
    "email": email,
    "otp": otp,
    "profile_image": profileImage,
    "deleted": deleted,
    "verified": verified,
    "_id": id,
    "created_date": createdDate.toIso8601String(),
    "__v": v,
  };
}

class Position {
  Position({
    this.type,
    this.coordinates,
  });

  String type;
  List<int> coordinates;

  factory Position.fromJson(Map<String, dynamic> json) => Position(
    type: json["type"],
    coordinates: List<int>.from(json["coordinates"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "coordinates": List<dynamic>.from(coordinates.map((x) => x)),
  };
}
