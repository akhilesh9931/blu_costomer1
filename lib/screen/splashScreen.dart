import 'dart:async';
import 'package:blu_customer/Models/otpverify.dart';
import 'package:blu_customer/screen/BottomNavigationBar.dart';
import 'package:flutter/material.dart';
import 'package:blu_customer/screen/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

String finalOtp;

class SplashScreenPage extends StatefulWidget {
  // final bool isSignedIn;
  // final VerifyOtpResponse verifyOtpResponse;
  // SplashScreenPage(this.isSignedIn ,this.verifyOtpResponse,);

  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {

  Future getvalidationData()async{
    final SharedPreferences sharedPreferences=await SharedPreferences.getInstance();
    var otp= sharedPreferences.getString('user_otp');
    setState(() {
      finalOtp= otp;
    });
    print(finalOtp);

  }
  bool isSplashTimeout;
  startTime() async {
    return new Timer(
        new Duration(
          milliseconds: 4200,
        ), () {
      setState(() {
        isSplashTimeout = true;
      });
    }); // duration of splash to show
  }

  @override
  void initState() {
    getvalidationData().whenComplete(() async{
      isSplashTimeout = false;
      startTime();

    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return
       isSplashTimeout
         ? finalOtp!=null?MyNavigationBar():Login():
         new Image.asset(
      'assets/flash-vs2.jpg',
      fit: BoxFit.cover,
    );
  }
}
