// To parse this JSON data, do
//
//     final profileResponse = profileResponseFromJson(jsonString);

import 'dart:convert';

ProfileResponse profileResponseFromJson(String str) => ProfileResponse.fromJson(json.decode(str));

String profileResponseToJson(ProfileResponse data) => json.encode(data.toJson());

class ProfileResponse {
  ProfileResponse({
    this.status,
    this.message,
    this.token,
    this.fullName,
    this.pincode,
    this.mobile,
    this.email,
    this.address,
    this.village,
    this.postOffice,
    this.thana,
    this.state,
    this.district,
  });

  String status;
  String message;
  String token;
  String fullName;
  String pincode;
  String mobile;
  String email;
  String address;
  String village;
  String postOffice;
  String thana;
  String state;
  String district;

  factory ProfileResponse.fromJson(Map<String, dynamic> json) => ProfileResponse(
    status: json["status"],
    message: json["message"],
    token: json["token"],
    fullName: json["full_name"],
    pincode: json["pincode"],
    mobile: json["mobile"],
    email: json["email"],
    address: json["address"],
    village: json["village"],
    postOffice: json["post_office"],
    thana: json["thana"],
    state: json["state"],
    district: json["district"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "token": token,
    "full_name": fullName,
    "pincode": pincode,
    "mobile": mobile,
    "email": email,
    "address": address,
    "village": village,
    "post_office": postOffice,
    "thana": thana,
    "state": state,
    "district": district,
  };
}
