import 'dart:convert';
// import 'dart:ffi';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'dart:typed_data';
import 'dart:io' show Platform;

class CustomView {
  BuildContext context;

  static Widget buildDropDown({
    BuildContext context,
    String inputValue,
    List<String> list,
    String text,
    Function onchanged,
    Function fn1,
    ValueKey key,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10.0),
      margin: const EdgeInsets.only(top: 20, left: 20, right: 20),
      width: MediaQuery.of(context).size.width,
      // padding: EdgeInsets.only(left: 3, right: 10),
      decoration: BoxDecoration(
        color: Color(0xffbdd5f1),
        borderRadius: BorderRadius.all(Radius.circular(6.0)),
      ),
      // border: Border.all(color: Colors.blue, width: 1)),
      child: DropdownButtonHideUnderline(
        // key: key,
        child: DropdownButton<String>(
          key: key,
          style:TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 15,
          ),
          // isDense: true,
          value: inputValue,
          autofocus: true,
          isExpanded: true,
          hint: new Container(
              padding: EdgeInsets.only(left: 5),
              child: FittedBox(
                child: Text(
                  text == null ? 'Value' : text,
                  style:TextStyle(
                   fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
              )),
          icon: Container(
            height: 15.0,
            width: 15.0,
            child: Image.asset(
              'assets/icons/DropDown-Icon-01.png',
            ),
          ),
          iconSize: 20,
          onChanged: onchanged,
          items: list.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              onTap: fn1,

              value: value,
              child: Container(
                  padding: EdgeInsets.only(left: 15),
                  child: Text(
                    value,
                    style:TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15,

                      color: Color(0xff2d1f76),
                    ),
                  )),
            );
          }).toList(),
        ),
      ),
    );
  }

}