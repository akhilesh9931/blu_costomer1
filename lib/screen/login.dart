import 'package:blu_customer/Models/loginResponse.dart';
import 'package:blu_customer/Models/signUpResponse.dart';
import 'package:blu_customer/Network/api_call.dart';
import 'package:blu_customer/screen/loginOtp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:blu_customer/screen/signup.dart';



class Login extends StatefulWidget {

  final SignUpResponse signUpResponse;
  const Login({Key key, this.signUpResponse}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _form = GlobalKey<FormState>();
  final TextEditingController mobileController = TextEditingController();

  Future<void> _getotp() async {
    final isValid = _form.currentState.validate();
    if (!isValid) {
      return;
    }

    else{
      mobileNo=mobileController.text;
      LoginResponse loginResponse = await ApiCall.postlogin(
       mobile: mobileNo
      );
      if (loginResponse == null) {
        print('data not  sent or error');
      }
      else{
        if (loginResponse.status == "success") {
          print(loginResponse.status);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => loginOtp(otpData,mobileNo)),
          );

          setState(() {
            isLoading = false;
            otpData = loginResponse.otp.toString();
            print(otpData);
            print(mobileNo);
            response =
                loginResponse.status;
            // print("signupResponse of it is:"+ signUpResponse)
          });
        }
      }
    }
  }

  List signUpData;
  String response;
  bool isLoading;
  String otpData, statusData, message, mobileNo, tokenCall;
  String number;
  String email;

  @override
  // void initState(){
  //   super.initState();
  //   mobile = '';
  //   email = '';
  //
  // }

  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(top: 70),
          color: Colors.white,
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.only(right: 60),
                // color: Colors.blue,
                width: 500,
                height: 200,

                alignment: Alignment.center,
                child: Image.asset("assets/loginlogo.png"),

              ),
              Container(
                // color: Colors.yellow,
                  width: 500,
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        // padding: const EdgeInsets.all(10),
                        width: 120,
                        height: 40,
                        //color: Colors.red,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(22.0),
                              side: BorderSide(color: Colors.black)),
                          onPressed: () {},
                          color: Colors.white,
                          textColor: Colors.black,
                          child: Text("LOGIN",
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold
                            ),),

                        ),

                      ),
                      Container(
                        // padding: const EdgeInsets.all(10),
                        width: 120,
                        height: 40,
                        //color: Colors.red,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(22.0),
                              side: BorderSide(color: Colors.black)),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => signup()),
                            );
                          },
                          color: Colors.white,
                          textColor: Colors.black,
                          child: Text("SIGN UP",
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold
                            ),),

                        ),

                      ),


                    ],
                  )
              ),

              Container(
                  padding: const EdgeInsets.only(top: 10,right: 20,left: 20),

                  //color: Colors.green,
                  width: 500,
                  height: 290,
                  child: Form(
                    key: _form,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          // padding: const EdgeInsets.all(20),
                          child: TextFormField(
                            controller: mobileController,
                            autocorrect: true,
                            validator: (text) {
                              if (!(text.length == 10) && text.isNotEmpty) {
                                return "Enter valid 10 digit mobile number";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: "Mobile Number",
                              prefixIcon: Icon(Icons.tablet_android_sharp),
                              hintStyle: TextStyle(color: Colors.grey),
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.black, width: 1),

                              ),
                            ),

                          ),
                        ),

                        Container(
                           padding: const EdgeInsets.only(top: 10),
                          width: 500,
                          height: 60,
                          //color: Colors.red,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                side: BorderSide(color: Colors.black)),
                            onPressed: () => _getotp(),
                            color: Colors.indigo[800],
                            textColor: Colors.black,
                            child: Text("GET OTP",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold
                              ),),

                          ),

                        ),




                      ],
                    ),
                  )
              ),
              Container(
                //padding: const EdgeInsets.only(top: 70),
                //color: Colors.blue,
                alignment: Alignment.bottomRight,
                // width: 500,
                //height: 300,
                child: Image.asset("assets/wave.png"),
              )

            ],
          ),
        ),
      ),
    );
  }
}
