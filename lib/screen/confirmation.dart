import 'package:blu_customer/Models/order.dart';
import 'package:blu_customer/screen/cart_items.dart';
import 'package:blu_customer/screen/orderSuccesful.dart';
import 'package:flutter/cupertino.dart';
import 'package:blu_customer/Models/cartResponse.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class confirmation extends StatefulWidget {
  //static const routeName ='/confirmation';

  @override
  _confirmationState createState() => _confirmationState();
}

class _confirmationState extends State<confirmation> {
  @override
  Widget build(BuildContext context) {
    final cart=Provider.of<Cart>(context,);
    return Scaffold(
      appBar: AppBar(
      centerTitle: true,
      leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: (){
            Navigator.pop(context,true);
          }
      ),
      backgroundColor:Colors.indigo[800],
      title: Container(
          padding: const EdgeInsets.only(top: 10,bottom: 5),
          child: Text('Checkout', style: TextStyle(fontSize: 28.0,fontWeight: FontWeight.bold),)),
      actions: <Widget>[
        IconButton(
          iconSize: 35,
          icon: const Icon(Icons.notifications),
          tooltip: 'Show Snackbar',
          onPressed: () {

          },
        ),
      ],
    ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Container(
                // color: Colors.red,width: 350,
                //height: 50,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text("Shipping",style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.indigo

                      ),),
                      decoration:BoxDecoration(

                          border: Border(
                              bottom: BorderSide(width: 3,color: Colors.green)
                          )
                      ) ,
                      padding: EdgeInsets.all(16),
                    ),
                    Container(
                      child: Text("Payment",style: TextStyle(
                          fontSize: 18,
                          color: Colors.indigo,
                          fontWeight: FontWeight.bold
                      ),),
                      decoration:BoxDecoration(
                          border: Border(
                              bottom: BorderSide(width: 3,color: Colors.green)
                          )
                      ),
                      padding: EdgeInsets.all(16),
                    ),
                    Container(
                      child: Text("Confirmation",style: TextStyle(
                          fontSize: 18,
                          color: Colors.indigo,
                          fontWeight: FontWeight.bold

                      ),),
                      decoration:BoxDecoration(
                          border: Border(
                              bottom: BorderSide(width: 3,color: Colors.green)
                          )
                      ),
                      padding: EdgeInsets.all(16),
                    )
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 20,right: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 10),

                     padding: const EdgeInsets.all(10),
                     // color:Colors.red,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text('Shipping to',style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold
                            ),),
                          ),
                          Container(
                            child:Text('Edit',style: TextStyle(
                              fontSize: 12,
                                color: Colors.indigo
                            ),)
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(3),
                      child: Text('John Smith',style: TextStyle(
                        fontSize: 14,
                        color: Colors.grey[700]
                      ),),
                    ),
                    Container(
                      padding: const EdgeInsets.all(3),
                      child: Text('701,Block-B,siddhi Vinayak Tower',style: TextStyle(
                          fontSize: 14,
                          color: Colors.grey[700]
                      ),),
                    ),
                    Container(
                      padding: const EdgeInsets.all(3),
                      child: Text('Ahmedabad-345001,Gujrat INDIA +910100000,',style: TextStyle(
                          fontSize: 14,
                          color: Colors.grey[700]
                      ),),
                    ),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(left: 5,right: 5),
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 10),
                      decoration: BoxDecoration(
                          //rcolor:Colors.red,
                          border: Border(
                              top: BorderSide(width: 1.0,color: Colors.grey,)
                          )
                      ),
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: const EdgeInsets.only(left: 5,right: 5),
                            child: Text('Your order',style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500
                            ),),
                          ),
                          Container(
                              child:Text('Edit',style: TextStyle(
                                fontSize: 12,
                                color: Colors.indigo
                              ),)
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: 300,
                      color: Colors.grey[100],
                      child: Expanded(
                        child: ListView.builder(
                          scrollDirection: Axis.vertical,
                          itemCount: cart.items.length,
                          itemBuilder: (ctx,i) => CartItems(
                            cart.items.values.toList()[i].id,
                            cart.items.keys.toList()[i],
                            cart.items.values.toList()[i].price,
                            cart.items.values.toList()[i].quantity,
                            cart.items.values.toList()[i].name,
                            cart.items.values.toList()[i].image,


                          ),
                          // {
                          //     // return Container(
                          //     //   decoration: BoxDecoration(
                          //     //       color: Colors.grey[300],
                          //     //       border: Border(
                          //     //         bottom: BorderSide(width: 2.0, color: Colors.grey[400]),
                          //     //       )
                          //     //   ),
                          //     //   margin: const EdgeInsets.only(left: 10,right: 5),
                          //     //   padding:  const EdgeInsets.all(13),
                          //     //   child: Row(
                          //     //     mainAxisAlignment: MainAxisAlignment.start,
                          //     //     children: [
                          //     //       Container(
                          //     //         decoration: BoxDecoration(
                          //     //             color: Colors.white,
                          //     //             borderRadius: BorderRadius.circular(7)
                          //     //         ),
                          //     //         padding:  const EdgeInsets.all(15),
                          //     //         child: Image.network('http://139.59.75.40:4040/uploads/product/${cart.items.values.toList()[index].image}',fit: BoxFit.fill,height: 65,width: 65,),
                          //     //       ),
                          //     //       Container(
                          //     //         width:200,
                          //     //
                          //     //         margin: const EdgeInsets.only(left:10),
                          //     //         padding:  const EdgeInsets.all(5),
                          //     //         child: Column(
                          //     //           mainAxisAlignment: MainAxisAlignment.start,
                          //     //           crossAxisAlignment: CrossAxisAlignment.start,
                          //     //           children: [
                          //     //             Container(
                          //     //                 padding:  const EdgeInsets.all(3),
                          //     //                 child: Text(cart.items.values.toList()[index].name,style: TextStyle(
                          //     //                     fontSize: 15,
                          //     //                     fontWeight: FontWeight.bold
                          //     //                 ),)
                          //     //             ),
                          //     //             Container(
                          //     //                 //padding:  const EdgeInsets.all(1),
                          //     //                 //alignment: Alignment.bottomRight,
                          //     //                 child: Text(cart.items.values.toList()[index].name,style: TextStyle(
                          //     //                     color: Colors.grey[700]
                          //     //                 ),)
                          //     //             ),
                          //     //             Container(
                          //     //                 padding:  const EdgeInsets.all(1),
                          //     //                 child: Row(
                          //     //                   children: [
                          //     //                     Container(
                          //     //                       child: Text("\u20B9",style: TextStyle(
                          //     //                           color: Colors.indigo[800]
                          //     //                       ),),
                          //     //                     ),
                          //     //                     Container(
                          //     //
                          //     //                       child: Text(cart.items.values.toList()[index].price.toString(),style: TextStyle(
                          //     //                           color: Colors.indigo[800]
                          //     //                       ),),
                          //     //                     ),
                          //     //
                          //     //                   ],
                          //     //                 )
                          //     //             ),
                          //     //             Container(
                          //     //                 child:Row(
                          //     //                   mainAxisAlignment: MainAxisAlignment.start,
                          //     //                   children: [
                          //     //                     Container(
                          //     //                       alignment: Alignment.center,
                          //     //                       height: 20,
                          //     //                       width: 20,
                          //     //                       color: Colors.green,
                          //     //                       child: Text("-",style: TextStyle(
                          //     //                           color: Colors.white,fontWeight: FontWeight.bold
                          //     //                       ),),
                          //     //                     ),
                          //     //                     Container(
                          //     //                       alignment: Alignment.center,
                          //     //                       height: 20,
                          //     //                       width: 20,
                          //     //                       child: Text(cart.items.values.toList()[index].quantity.toString(),style: TextStyle(
                          //     //                           color: Colors.black
                          //     //                       ),),
                          //     //                     ),
                          //     //                     Container(
                          //     //                       alignment: Alignment.center,
                          //     //                       height: 20,
                          //     //                       width: 20,
                          //     //                       color: Colors.green,
                          //     //                       child: Text("+",style: TextStyle(
                          //     //                           color: Colors.white,fontWeight: FontWeight.bold
                          //     //                       ),),
                          //     //                     ),
                          //     //
                          //     //                   ],
                          //     //                 )
                          //     //             ),
                          //     //           ],
                          //     //         ),
                          //     //
                          //     //       ),
                          //     //       Container(
                          //     //         margin:const EdgeInsets.only(left: 7,bottom: 50) ,
                          //     //         child: Expanded (
                          //     //           child: IconButton(
                          //     //               icon: Icon(Icons.delete),
                          //     //               iconSize: 35,
                          //     //               color: Colors.red,
                          //     //               splashColor: Colors.purple,
                          //     //               onPressed: () {
                          //     //                 Provider.of<Cart>(context).removeItem(cart.items.values.toList()[index].id);
                          //     //               print(cart.items.values.toList()[index].id);
                          //     //             },
                          //     //           ),
                          //     //         )
                          //     //       ),
                          //     //     ],
                          //     //   ),
                          //     //
                          //     // );
                          //
                          // }

                        ),
                      ),
                    ),
                    // Container(
                    //   // decoration: BoxDecoration(
                    //   //
                    //   //     border: Border(
                    //   //       bottom: BorderSide(width: 3.0, color: Colors.grey[400]),
                    //   //     )
                    //   // ),
                    //   margin: const EdgeInsets.only(left: 10,right: 10),
                    //   padding:  const EdgeInsets.all(13),
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.start,
                    //     children: [
                    //       Container(
                    //         decoration: BoxDecoration(
                    //             color: Colors.white,
                    //             borderRadius: BorderRadius.circular(7),
                    //           border: Border.all(width:0.5,color: Colors.black54)
                    //         ),
                    //         padding:  const EdgeInsets.all(12),
                    //         child: Image.asset('assets/banner.png',fit: BoxFit.fill,height: 50,width: 50,),
                    //       ),
                    //       Container(
                    //         margin: const EdgeInsets.only(left:10),
                    //         padding:  const EdgeInsets.all(5),
                    //         child: Column(
                    //           mainAxisAlignment: MainAxisAlignment.spaceAround,
                    //           crossAxisAlignment: CrossAxisAlignment.start,
                    //           children: [
                    //             Container(
                    //                 padding:  const EdgeInsets.all(3),
                    //                 child: Text("Whisper Ultra XL fdg Plus",style: TextStyle(
                    //                     fontSize: 15,
                    //                     fontWeight: FontWeight.bold
                    //                 ),)
                    //             ),
                    //             Container(
                    //                 padding:  const EdgeInsets.all(1),
                    //                 alignment: Alignment.bottomRight,
                    //                 child: Text("Size: pads",style: TextStyle(
                    //                     color: Colors.grey[700]
                    //                 ),)
                    //             ),
                    //             Container(
                    //                 padding:  const EdgeInsets.all(1),
                    //                 child: Row(
                    //                   children: [
                    //                     Container(
                    //                       child: Text("\u20B9",style: TextStyle(
                    //                           color: Colors.indigo[800],fontSize: 20
                    //                       ),),
                    //                     ),
                    //                     Container(
                    //
                    //                       child: Text("1299",style: TextStyle(
                    //                           color: Colors.indigo[800],
                    //                         fontSize: 20
                    //
                    //                       ),),
                    //                     ),
                    //
                    //                   ],
                    //                 )
                    //             ),
                    //           ],
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    //
                    // ),


                  ],
                ),

              ),
              Container(
                padding: const EdgeInsets.only(left: 20,right: 20),
                child: Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 20),
                      decoration: BoxDecoration(
                        //rcolor:Colors.red,
                          border: Border(
                              top: BorderSide(width: 0.5,color: Colors.grey,)
                          )
                      ),
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text('Payment summary',style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w500
                            ),),
                          ),
                          Container(
                              child:Text('Edit',style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.indigo
                              ),)
                          ),
                        ],
                      ),
                    ),

                    Container(
                      decoration: BoxDecoration(
                        //rcolor:Colors.red,
                          border: Border(
                              top: BorderSide(width: 0.5,color: Colors.grey,)
                          )
                      ),
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text('Order Total',style: TextStyle(
                              fontSize: 16
                            ),),
                          ),
                          Container(
                            child: Text(cart.totalAmount.toString(),style: TextStyle(
                                fontSize: 16
                            ),),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        //rcolor:Colors.red,
                          border: Border(
                              top: BorderSide(width: 0.5,color: Colors.grey,)
                          )
                      ),
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text('Delivery charge',style: TextStyle(
                                fontSize: 16
                            ),),
                          ),
                          Container(
                            child: Text('Free',style: TextStyle(
                                fontSize: 16,
                              color: Colors.green
                            ),),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text('Order Total',style: TextStyle(
                                fontSize: 16,
                              fontWeight: FontWeight.bold
                            ),),
                          ),
                          Container(
                            child: Text(cart.totalAmount.toString(),style: TextStyle(
                                fontSize: 18,
                              color: Colors.green
                            ),),
                          ),
                        ],
                      ),
                    ),

                  ],
                ),

              ),
              Container(
                height: 50,
                alignment: Alignment.center,
                color: Colors.indigo[900],
                child: InkWell(
                  onTap: (){
                    Provider.of<Orders>(context,listen: false).addOrder(
                      cart.items.values.toList(),
                      cart.totalAmount,
                    );
                    cart.clear();

                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context)=>orderSuccess(),
                        )
                    );


                  },
                  child: Text('ORDER NOW',style: TextStyle(
                    fontSize: 20,
                    color: Colors.white
                  ),),
                ),
              ),
            ],
          )
        ),
      ),
    );
  }
}
