// To parse this JSON data, do
//
//     final productDeatailResponse = productDeatailResponseFromJson(jsonString);

import 'dart:convert';

ProductDeatailResponse productDeatailResponseFromJson(String str) => ProductDeatailResponse.fromJson(json.decode(str));

String productDeatailResponseToJson(ProductDeatailResponse data) => json.encode(data.toJson());

class ProductDeatailResponse {
  ProductDeatailResponse({
    this.status,
    this.token,
    this.productBaseUrl,
    this.result,
  });

  String status;
  String token;
  String productBaseUrl;
  Result result;

  factory ProductDeatailResponse.fromJson(Map<String, dynamic> json) => ProductDeatailResponse(
    status: json["status"],
    token: json["token"],
    productBaseUrl: json["product_base_url"],
    result: Result.fromJson(json["result"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "token": token,
    "product_base_url": productBaseUrl,
    "result": result.toJson(),
  };
}

class Result {
  Result({
    this.parentGroup,
    this.childGroup,
    this.brand,
    this.unit,
    this.state,
    this.active,
    this.qty,
    this.deleted,
    this.id,
    this.name,
    this.unitValue,
    this.vendor,
    this.manufacturingPrice,
    this.sellingPriceOrg,
    this.sellingPrice,
    this.cgst,
    this.sgst,
    this.igst,
    this.batchCode,
    this.totalProductPack,
    this.totalPackBox,
    this.boxHeight,
    this.boxLength,
    this.boxWidth,
    this.city,
    this.image,
    this.createdDate,
    this.v,
  });

  String parentGroup;
  String childGroup;
  String brand;
  String unit;
  String state;
  int active;
  int qty;
  int deleted;
  String id;
  String name;
  String unitValue;
  String vendor;
  String manufacturingPrice;
  String sellingPriceOrg;
  String sellingPrice;
  String cgst;
  String sgst;
  String igst;
  String batchCode;
  String totalProductPack;
  String totalPackBox;
  String boxHeight;
  String boxLength;
  String boxWidth;
  String city;
  String image;
  DateTime createdDate;
  int v;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    parentGroup: json["parent_group"],
    childGroup: json["child_group"],
    brand: json["brand"],
    unit: json["unit"],
    state: json["state"],
    active: json["active"],
    qty: json["qty"],
    deleted: json["deleted"],
    id: json["_id"],
    name: json["name"],
    unitValue: json["unit_value"],
    vendor: json["vendor"],
    manufacturingPrice: json["manufacturing_price"],
    sellingPriceOrg: json["selling_price_org"],
    sellingPrice: json["selling_price"],
    cgst: json["cgst"],
    sgst: json["sgst"],
    igst: json["igst"],
    batchCode: json["batch_code"],
    totalProductPack: json["total_product_pack"],
    totalPackBox: json["total_pack_box"],
    boxHeight: json["box_height"],
    boxLength: json["box_length"],
    boxWidth: json["box_width"],
    city: json["city"],
    image: json["image"],
    createdDate: DateTime.parse(json["created_date"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "parent_group": parentGroup,
    "child_group": childGroup,
    "brand": brand,
    "unit": unit,
    "state": state,
    "active": active,
    "qty": qty,
    "deleted": deleted,
    "_id": id,
    "name": name,
    "unit_value": unitValue,
    "vendor": vendor,
    "manufacturing_price": manufacturingPrice,
    "selling_price_org": sellingPriceOrg,
    "selling_price": sellingPrice,
    "cgst": cgst,
    "sgst": sgst,
    "igst": igst,
    "batch_code": batchCode,
    "total_product_pack": totalProductPack,
    "total_pack_box": totalPackBox,
    "box_height": boxHeight,
    "box_length": boxLength,
    "box_width": boxWidth,
    "city": city,
    "image": image,
    "created_date": createdDate.toIso8601String(),
    "__v": v,
  };
}
