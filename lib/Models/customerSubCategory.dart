// To parse this JSON data, do
//
//     final customersubCategoryResponse = customersubCategoryResponseFromJson(jsonString);

import 'dart:convert';

CustomersubCategoryResponse customersubCategoryResponseFromJson(String str) => CustomersubCategoryResponse.fromJson(json.decode(str));

String customersubCategoryResponseToJson(CustomersubCategoryResponse data) => json.encode(data.toJson());

class CustomersubCategoryResponse {
  CustomersubCategoryResponse({
    this.status,
    this.token,
    this.categoryBaseUrl,
    this.result,
  });

  String status;
  String token;
  String categoryBaseUrl;
  List<Result> result;

  factory CustomersubCategoryResponse.fromJson(Map<String, dynamic> json) => CustomersubCategoryResponse(
    status: json["status"],
    token: json["token"],
    categoryBaseUrl: json["category_base_url"],
    result: List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "token": token,
    "category_base_url": categoryBaseUrl,
    "result": List<dynamic>.from(result.map((x) => x.toJson())),
  };
}

class Result {
  Result({
    this.active,
    this.deleted,
    this.id,
    this.parent,
    this.name,
    this.image,
    this.createdDate,
    this.v,
    this.parentName,
  });

  int active;
  int deleted;
  String id;
  String parent;
  String name;
  String image;
  DateTime createdDate;
  int v;
  String parentName;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    active: json["active"],
    deleted: json["deleted"],
    id: json["_id"],
    parent: json["parent"],
    name: json["name"],
    image: json["image"],
    createdDate: DateTime.parse(json["created_date"]),
    v: json["__v"],
    parentName: json["parent_name"],
  );

  Map<String, dynamic> toJson() => {
    "active": active,
    "deleted": deleted,
    "_id": id,
    "parent": parent,
    "name": name,
    "image": image,
    "created_date": createdDate.toIso8601String(),
    "__v": v,
    "parent_name": parentName,
  };
}
