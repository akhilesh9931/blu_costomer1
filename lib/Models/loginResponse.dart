import 'dart:convert';

LoginResponse loginResponseFromJson(String str) => LoginResponse.fromJson(json.decode(str));

String loginResponseToJson(LoginResponse data) => json.encode(data.toJson());

class LoginResponse {
  LoginResponse({
    this.mobile,
    this.status,
    this.otp,
    this.message,
  });

  String mobile;
  String status;
  int otp;
  String message;

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
    mobile: json["mobile"],
    status: json["status"],
    otp: json["OTP"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "mobile": mobile,
    "status": status,
    "OTP": otp,
    "message": message,
  };
}
