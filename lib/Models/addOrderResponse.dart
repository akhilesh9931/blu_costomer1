// To parse this JSON data, do
//
//     final addOrderResponse = addOrderResponseFromJson(jsonString);

import 'dart:convert';

AddOrderResponse addOrderResponseFromJson(String str) => AddOrderResponse.fromJson(json.decode(str));

String addOrderResponseToJson(AddOrderResponse data) => json.encode(data.toJson());

class AddOrderResponse {
  AddOrderResponse({
    this.status,
    this.message,
    this.token,
    this.products,
    this.id,
    this.address,
    this.paymentMethod,
    this.transactionId,
    this.totalAmount,
  });

  String status;
  String message;
  String token;
  List<Product> products;
  String id;
  String address;
  String paymentMethod;
  String transactionId;
  String totalAmount;

  factory AddOrderResponse.fromJson(Map<String, dynamic> json) => AddOrderResponse(
    status: json["status"],
    message: json["message"],
    token: json["token"],
    products: List<Product>.from(json["products"].map((x) => Product.fromJson(x))),
    id: json["id"],
    address: json["address"],
    paymentMethod: json["payment_method"],
    transactionId: json["transaction_id"],
    totalAmount: json["total_amount"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "token": token,
    "products": List<dynamic>.from(products.map((x) => x.toJson())),
    "id": id,
    "address": address,
    "payment_method": paymentMethod,
    "transaction_id": transactionId,
    "total_amount": totalAmount,
  };
}

class Product {
  Product({
    this.product,
    this.qty,
  });

  String product;
  String qty;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    product: json["product"],
    qty: json["qty"],
  );

  Map<String, dynamic> toJson() => {
    "product": product,
    "qty": qty,
  };
}
