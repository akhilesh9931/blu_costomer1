import 'dart:async';
import 'package:blu_customer/Models/addressListRespose.dart';
import 'package:blu_customer/Models/adressResponse.dart';
import 'package:blu_customer/Models/categoryResponse.dart';
import 'package:blu_customer/Models/editProfileResponse.dart';
import 'package:blu_customer/Models/homeResponse.dart';
import 'package:blu_customer/Models/loginResponse.dart';
import 'package:blu_customer/Models/otpverify.dart';
import 'package:blu_customer/Models/productDeatailResponse.dart';
import 'package:blu_customer/Models/profileResponse.dart';
import 'package:blu_customer/Models/signUpResponse.dart';
import 'package:blu_customer/Models/stateResponse.dart';
import 'package:blu_customer/Models/subCategoryResponse.dart';
import 'package:blu_customer/screen/category.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:dio/dio.dart';
import 'api_constants.dart';
import 'package:flutter/foundation.dart';

class ApiCall {
  static Map<String, dynamic> verifyData;

  static String token, tokenCall;
  static Map<String, dynamic> stateData;
  static List<dynamic> stateList;
  static List<String> listData = [];
  static Map<String, dynamic> loginData;
  static String otpByLogin, statusData;
  static List districData;
  static List<String> districtList = [];
  static Map<String, dynamic> aboutData;

  //this api is for profile

  static Future<VerifyOtpResponse> getprofile() async {
    final response = await http.post(Uri.encodeFull(AppConstants.otpverfiy),
        headers: {
          "accept": "text/plain",
          // "authorization": "$token",
        },
        body: json.encode({
        }));
    if (response.statusCode == 200) {
      final String responseString = response.body;
      return verifyOtpResponseFromJson(responseString);
    } else {
      return null;
    }
  }



  //this api for the home page
  static Future<HomeResponse> fetchHomeResponse() async {
    HomeResponse homeResponse = HomeResponse();
    var response = await http.post(Uri.encodeFull(AppConstants.home),);
    print(response.body);
    if (response.statusCode == 200) {
      var data=HomeResponse.fromJson(json.decode(response.body));
      return data;
      // getSupervisorModel = json.decode(response.body);
    } else {
      print("There is an error");
    }
    return homeResponse;
  }

  //this is for the  statedistrict api

  static Future<StateResponse> getStateDistList() async {
    StateResponse stateResponse = StateResponse();
    var response = await http.post(Uri.encodeFull(AppConstants.stateDist),);
    print(response.body);
    if (response.statusCode == 200) {
      var data=StateResponse.fromJson(json.decode(response.body));
      print(data);



      return data;
      // getSupervisorModel = json.decode(response.body);
    } else {
      print("There is an error");
    }
    return stateResponse;
  }

  //this is for the productdetail page
  static Future<ProductDeatailResponse> fetchProductDeatailResponse(
      @required id,
      ) async {
    ProductDeatailResponse productDeatailResponse = ProductDeatailResponse();
    var response = await http.post(Uri.encodeFull(AppConstants.productDeatail),
    body: json.encode({
      "product" :id,
    }),
    );
    print(response.body);
    if (response.statusCode == 200) {
      var data=ProductDeatailResponse.fromJson(json.decode(response.body));
      return data;
      // getSupervisorModel = json.decode(response.body);
    } else {
      print("There is an error");
    }
    return productDeatailResponse;
  }

  //to get the product for grid view and category

  static Future<CategoryResponse> fetchCategoryResponse() async {
    CategoryResponse categoryResponse = CategoryResponse();
    var response = await http.post(Uri.encodeFull(AppConstants.category),);
    print(response.body);
    if (response.statusCode == 200) {
      var data=CategoryResponse.fromJson(json.decode(response.body));
     return data;
      // getSupervisorModel = json.decode(response.body);
    } else {
      print("There is an error");
    }
    return categoryResponse;
  }


  //this  Api is for the sub category

  static Future<SubCategoryResponse> fetchCategoryProductResponse(
      @required id
      ) async {
    SubCategoryResponse subCategoryResponse = SubCategoryResponse();
    var response = await http.post(Uri.encodeFull(AppConstants.categoryProduct),
       body: json.encode({
        "category": id,
        }));


    print(response.body);
    if (response.statusCode == 200) {
      var data=SubCategoryResponse.fromJson(json.decode(response.body));
      return data;
      // getSupervisorModel = json.decode(response.body);
    } else {
      print("There is an error");
    }
    return subCategoryResponse;
  }

  //this api is for the address list
  static Future<AddressListResponse> fetchaddressListResponse(
      @required id
      ) async {
    AddressListResponse addressListResponse = AddressListResponse();
    var response = await http.post(Uri.encodeFull(AppConstants.addressList),
        body: json.encode({
          "id": id,
        }));


    print(response.body);
    if (response.statusCode == 200) {
      var data=AddressListResponse.fromJson(json.decode(response.body));
      return data;
      // getSupervisorModel = json.decode(response.body);
    } else {
      print("There is an error");
    }
    return addressListResponse;
  }

  // this api is for the customer sub category
  static Future<SubCategoryResponse> fetchCustomersubCategoryResponse() async {
    SubCategoryResponse subCategoryResponse = SubCategoryResponse();
    var response = await http.post(Uri.encodeFull(AppConstants.customerSubCategory),);
    print(response.body);
    if (response.statusCode == 200) {
      var data=SubCategoryResponse.fromJson(json.decode(response.body));
      return data;
      // getSupervisorModel = json.decode(response.body);
    } else {
      print("There is an error");
    }
    return subCategoryResponse;
  }


  //category api

  static Future<List<CategoryResponse>> getCategory() async {
    try {
      final response = await http.post(AppConstants.category,);
      if (response.statusCode == 200) {
        print(response.body);

        return compute(parse,response.body);
        //print(list);
      }else{
        throw Exception("Error");
      }
    } catch (e) {
      throw  Exception(e.toString());
    }
  }

  static List<CategoryResponse>parse(String responseBody){
    var list=json.decode(responseBody) as List<dynamic>;
    List<CategoryResponse>categoryResponseData=list.map((model) => CategoryResponse.fromJson(model)).toList();
    return categoryResponseData;
  }





//signup api
  static Future<SignUpResponse> postSignUp({String mobile,String email}) async {
    var body = jsonEncode({
      "mobile": mobile,
      "email": email,
    });
    final response = await http
        .post(Uri.encodeFull(AppConstants.signup),
        headers: {
               "accept": "text/plain",
               //"authorization": "$token",
            },
        body: body);
    print(response.body);
    if (response.statusCode == 200) {
      print(response.body);

      return SignUpResponse.fromJson(json.decode(response.body));
      // print("Status OK");
    } else {
      throw Exception('Failed to load post');
    }
  }
//this api is for api sent message for login
  static Future<LoginResponse> postlogin(
      {@required String mobile,}) async {
    final response = await http.post(Uri.encodeFull(AppConstants.otp),
        headers: {
          "accept": "text/plain",
         // "authorization": "$token",
        },
        body: json.encode({
          "mobile": mobile,
        }));
    if (response.statusCode == 200) {
      final String responseString = response.body;
      return loginResponseFromJson(responseString);
    } else {
      return null;
    }
  }
  //this api is for login  verfication otp
  static Future<VerifyOtpResponse> postverifyOtp(
      {@required String mobile,String otp}) async {
    final response = await http.post(Uri.encodeFull(AppConstants.otpverfiy),
        headers: {
          "accept": "text/plain",
          // "authorization": "$token",
        },
        body: json.encode({
          "mobile": mobile,
          "otp": otp,
        }));
    if (response.statusCode == 200) {
      final String responseString = response.body;
      return verifyOtpResponseFromJson(responseString);
    } else {
      return null;
    }
  }


  //this api is for the edit profile

  // static Future<EditProfileResponse> postEditProfile(
  //     {
  //       @required String id,
  //       @required String fullname,
  //       @required String mobile,
  //       @required String email,
  //       @required String address,
  //       @required String pincode,
  //       @required String city,
  //       @required String state,
  //
  //     }) async {
  //   final response = await http.post(Uri.encodeFull(AppConstants.editProfile),
  //       headers: {
  //         "accept": "text/plain",
  //         // "authorization": "$token",
  //       },
  //       body: json.encode({
  //         "_id": id,
  //         "full_name": fullname,
  //         "mobile": mobile,
  //         "email": email,
  //         "address": address,
  //         "district": city,
  //         "pincode": pincode,
  //         "state": state,
  //
  //       }));
  //   print(response);
  //   if (response.statusCode == 200) {
  //     final String responseString = response.body;
  //     return EditProfileResponse.fromJson(json.decode(responseString));
  //   } else {
  //     return null;
  //   }
  // }

  static Future<EditProfileResponse> postEditProfile({
    @required String id,
    @required String fullname,
    @required String mobile,
    @required String email,
    @required String address,
    @required String pincode,
    @required String city,
    @required String state,
  }) async {
    var body = jsonEncode({
      "_id": id,
      "full_name": fullname,
      "mobile": mobile,
      "email": email,
      "address": address,
      "district": city,
      "pincode": pincode,
      "state": state,
    });
    final response = await http
        .post(Uri.encodeFull(AppConstants.editProfile),
        headers: {
        //  "accept": "text/plain",
          //"authorization": "$token",
        },
        body: body);
    print(response.body);
    if (response.statusCode == 200) {
      print(response.body);

      return EditProfileResponse.fromJson(json.decode(response.body));
      // print("Status OK");
    } else {
      throw Exception('Failed to load post');
    }
  }

  //add address api

  static Future<AddressResponse> postaddAdress({
    @required String id,
    @required String level,
    @required String fullname,
    @required String mobile,
   // @required String email,
    @required String address,
    String address2,
    @required String pincode,
    @required String city,
    @required String state,
   // @required String district,
  }) async {
    var body = jsonEncode({
      "lebel": level,
      "contact_name": fullname,
      "contact_number": mobile,
      "customer": id,
      "address_1": address,
      "address_2": address2,
      "pincode": pincode,
      "state": state,
      "district": city,


      //"deleted": "",


    });
    final response = await http
        .post(Uri.encodeFull(AppConstants.addAddress),
        headers: {
         // "accept": "text/plain",
          //"authorization": "$token",
        },
        body: body);
    print(response.body);
    if (response.statusCode == 200) {
      print(response.statusCode);

      return AddressResponse.fromJson(json.decode(response.body));
      // print("Status OK");
    } else {
      throw Exception('Failed to load post');
    }
  }





  static var uri = AppConstants.baseUrl;

  static BaseOptions options = BaseOptions(
      baseUrl: uri,
      responseType: ResponseType.plain,
      connectTimeout: 30000,
      receiveTimeout: 30000,
      validateStatus: (code) {
        if (code >= 200) {
          return true;
        } else {
          return false;
        }
      });

  static Dio dio = Dio(options);

}
