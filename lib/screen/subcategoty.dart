import 'package:blu_customer/Models/subCategoryResponse.dart';
import 'package:blu_customer/Network/api_call.dart';
import 'package:blu_customer/screen/product_detail.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


String id;

class subcategory extends StatefulWidget {
  final String id;
  subcategory(this.id, {Key key}): super(key: key);


  // final String Id;
  // subcategory(this.Id, {Key key}): super(key: key);

  @override
  _subcategoryState createState() => _subcategoryState();
}

class _subcategoryState extends State<subcategory> {

  Future<SubCategoryResponse>_categoryProduct;

  // Future CategoryId()async{
  //   final SharedPreferences sharedPreferences=await SharedPreferences.getInstance();
  //   var Id= sharedPreferences.getString('Category_id');
  //   setState(() {
  //     id= Id;
  //   });
  //   print(id);
  //
  // }

  @override
  void initState() {
    // id=widget.Id;
   // CategoryId();

    print("Hello ravan your id you are genuis  and u hav the right to live in this world and your id is  ${widget.id}");
    _categoryProduct=ApiCall.fetchCategoryProductResponse(widget.id);
    super.initState();
  }

  circularProgress(){
    return Center(
      child: CircularProgressIndicator(),
    );
  }
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: SingleChildScrollView(
        child: Container(
          color: Colors.grey[300],
          child: Column(
            children: [
              // Container(
              //   //height: 200,
              //   padding: const EdgeInsets.all(12),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.end,
              //     children: [
              //       Container(
              //         margin: const EdgeInsets.only(right: 10),
              //         //padding: const EdgeInsets.all(5),
              //         decoration: BoxDecoration(
              //             color: Colors.white,
              //             borderRadius: BorderRadius.circular(10)
              //         ),
              //         child: Row(
              //           mainAxisAlignment: MainAxisAlignment.spaceAround,
              //           children: [
              //             Container(
              //               padding: const EdgeInsets.only(top:7,right: 5,left:12,bottom: 4),
              //               height: 33,
              //               child: Image.asset('assets/short.png'),
              //             ),
              //             Container(
              //               padding: const EdgeInsets.only(top: 6,right: 15,left:3),
              //               // width:60,
              //               height: 35,
              //               child: Text('Short',style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
              //             )
              //           ],
              //         ),
              //
              //       ),
              //       Container(
              //         decoration: BoxDecoration(
              //             color: Colors.white,
              //             borderRadius: BorderRadius.circular(10)
              //         ),
              //         child: Row(
              //           mainAxisAlignment: MainAxisAlignment.spaceAround,
              //           children: [
              //             Container(
              //               padding: const EdgeInsets.only(top:7,right: 5,left:12,bottom: 4),
              //               height: 30,
              //               child: Image.asset('assets/filter1.png'),
              //             ),
              //             Container(
              //               padding: const EdgeInsets.only(top: 6,right: 15,left:3),
              //               height: 35,
              //               child: Text('Filter',style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
              //             )
              //           ],
              //         ),
              //
              //       ),
              //     ],
              //   ),
              // ),
              //this is for the grid view
              Container(
                // color: Colors.red,
                height: 700,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      child: FutureBuilder(
                        future: _categoryProduct,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            print("Snapshot has data");
                            SubCategoryResponse model = snapshot.data;
                            //imagedata=widget.categoryResponse.result[1].image;
                            // Map<String, String> bodyData = snapshot.data;
                            return GridView.builder(
                                itemCount: model.result.length,
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 3,
                                    childAspectRatio: 3 /3,
                                    crossAxisSpacing: 5,
                                    mainAxisSpacing: 5),
                                itemBuilder: (ctx, index) {
                                  return

                                    Container(
                                      color: Colors.grey[300],
                                      child: Card(
                                        child: InkWell(
                                          onTap: (){
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (context)=>product_detail(model.result[index].id),
                                                )
                                            );
                                          },
                                          child: Container(
                                            child: Column(
                                              children: [
                                                Container(
                                                  //color: Colors.green,
                                                  alignment: Alignment.center,
                                                  // margin: const EdgeInsets.only(top:5,right: 10,left: 10),
                                                  padding: const EdgeInsets.only(top: 15,right: 15,left: 15),
                                                    child: Image.network('http://139.59.75.40:4040/uploads/product/${model.result[index].image}',fit: BoxFit.fill,),
                                                  //Image.asset('assets/banner.png',fit: BoxFit.fill),
                                                  //Image.network('http://139.59.75.40:4040/uploads/product_group/${model.result[index].image}',fit: BoxFit.fill,),
                                                  // decoration: BoxDecoration(
                                                  //  // borderRadius: BorderRadius.circular(18.0),
                                                  //   image: DecorationImage(
                                                  //     //image:AssetImage('assets/banner.png'),
                                                  //     image: NetworkImage(model.result[index].image),
                                                  //     fit: BoxFit.fill,
                                                  //   ),
                                                  // ),
                                                ),
                                                Container(
                                                  margin: const EdgeInsets.only(top: 16),
                                                  padding: const EdgeInsets.only(left: 5,right: 5),
                                                  width: 120,
                                                  child: Text(model.result[index].name,
                                                    style: TextStyle(
                                                        fontSize: 11,
                                                        fontWeight: FontWeight.bold

                                                    ),) ,
                                                ),
                                                Container(
                                                    margin: const EdgeInsets.only(top: 3,bottom: 1),
                                                    padding: const EdgeInsets.only(right: 10,left: 10),
                                                    child:Row(
                                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: [
                                                        Container(
                                                          child: Text("\u20B9"),
                                                        ),
                                                        Container(

                                                          child: Text(model.result[index].sellingPrice,style: TextStyle(
                                                              color: Colors.indigo[800]
                                                          ),),
                                                        ),
                                                        Container(
                                                          width: 15,
                                                          margin: const EdgeInsets.only(left: 40),
                                                          child: Image.asset('assets/love.png',fit: BoxFit.contain),

                                                        ),
                                                      ],
                                                    )
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                });
                          }
                          else{
                            return Center(
                                child: circularProgress(),
                            );
                          }
                         // return CircularProgressIndicator();
                        },
                      ),

                    ),
                  ],
                ),
              ),


            ],
          ),
        ),
      ),
    );
  }
}
