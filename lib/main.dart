import 'package:blu_customer/Models/cartResponse.dart';
import 'package:blu_customer/Models/order.dart';
import 'package:blu_customer/screen/BottomNavigationBar.dart';
import 'package:blu_customer/screen/confirmation.dart';
import 'package:blu_customer/screen/homepage.dart';
import 'package:blu_customer/screen/login.dart';
import 'package:blu_customer/screen/oder.dart';
import 'package:blu_customer/screen/signup.dart';
import 'package:blu_customer/screen/splashScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:blu_customer/screen/cart.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
       ChangeNotifierProvider(
         create:(ctx) => Cart(),
       ),
        ChangeNotifierProvider(
          create:(ctx) => Orders(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(

          primarySwatch: Colors.indigo,
        ),
        home: SplashScreenPage(),
        routes: {
          cart.routeName :(ctx)=> cart(),

          Order.routeName :(ctx)=> Order(),
        },
        //signup(),
        //MyNavigationBar(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}

