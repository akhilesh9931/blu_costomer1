import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:blu_customer/Models/cartResponse.dart';


class CartItems extends StatelessWidget {
  final String id;
  final String productId;
  final double price;
  final int quantity;
  final String name;
  final String image;
  CartItems(
      this.id,
      this.productId,
      this.price,
      this.quantity,
      this.name,
      this.image
      );

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key:  ValueKey(id),
      background: Container(
        color:Theme.of(context).errorColor,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size:40,

        ),
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 20),

      ),
      direction: DismissDirection.endToStart,
      onDismissed: (direction){
        Provider.of<Cart>(context,listen: false).removeItem(productId);

      },
      child: Card(
        //color: Colors.transparent,
        margin: EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 6,
        ),
        child: Padding(
          padding: EdgeInsets.all(9),
          //cart.items.values.toList()[index].image
          child: ListTile(
            leading: Image.network('http://139.59.75.40:4040/uploads/product/${image}',fit: BoxFit.fill,height: 65,width: 65,),
            title: Text(name),
            subtitle: Container(
             // color: Colors.green,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Quantity: ${quantity}'),
                  Text('Price : ${price}'),
                ],
              ),
            ),
            trailing: Text('Total:${(price*quantity)}',style: TextStyle(
              color: Colors.red
            ),),

          ),
        ),
      ),
    );
  }
}
