// To parse this JSON data, do
//
//     final categoryResponse = categoryResponseFromJson(jsonString);

import 'dart:convert';

CategoryResponse categoryResponseFromJson(String str) => CategoryResponse.fromJson(json.decode(str));

String categoryResponseToJson(CategoryResponse data) => json.encode(data.toJson());

class CategoryResponse {
  CategoryResponse({
    this.status,
    this.token,
    this.categoryBaseUrl,
    this.result,
  });

  String status;
  String token;
  String categoryBaseUrl;
  List<Result> result;

  factory CategoryResponse.fromJson(Map<String, dynamic> json) => CategoryResponse(
    status: json["status"],
    token: json["token"],
    categoryBaseUrl: json["category_base_url"],
    result: List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "token": token,
    "category_base_url": categoryBaseUrl,
    "result": List<dynamic>.from(result.map((x) => x.toJson())),
  };
}

class Result {
  Result({
    this.active,
    this.deleted,
    this.id,
    this.parent,
    this.name,
    this.image,
    this.createdDate,
    this.v,
  });

  int active;
  int deleted;
  String id;
  String parent;
  String name;
  String image;
  DateTime createdDate;
  int v;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    active: json["active"],
    deleted: json["deleted"],
    id: json["_id"],
    parent: json["parent"],
    name: json["name"],
    image: json["image"],
    createdDate: DateTime.parse(json["created_date"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "active": active,
    "deleted": deleted,
    "_id": id,
    "parent": parent,
    "name": name,
    "image": image,
    "created_date": createdDate.toIso8601String(),
    "__v": v,
  };
}
