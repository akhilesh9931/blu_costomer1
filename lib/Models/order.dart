import 'package:blu_customer/Models/cartResponse.dart';
import 'package:flutter/foundation.dart';


class OrderItems{
  final String id;
  final double amount;
  final List<CartItem> products;
  final DateTime dateTime;

  OrderItems({
    @required this.id,
    @required this.amount,
    @required this.products,
    @required this.dateTime
});
}

class Orders with ChangeNotifier{
  List<OrderItems> _order=[];

  List<OrderItems> get orders{
    return[..._order];
  }
 void addOrder(List<CartItem> cartProducts,double total){
    _order.insert(0,
        OrderItems(
            id: DateTime.now().toString(),
            amount: total,
            products: cartProducts,
            dateTime: DateTime.now()
        )
    );
    notifyListeners();
 }

}