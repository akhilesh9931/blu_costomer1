import 'package:blu_customer/Models/addressListRespose.dart';
import 'package:blu_customer/Models/subCategoryResponse.dart';
import 'package:blu_customer/Network/api_call.dart';
import 'package:blu_customer/screen/Payment.dart';
import 'package:blu_customer/screen/checkout.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class addressList extends StatefulWidget {
  @override
  _addressListState createState() => _addressListState();
}

class _addressListState extends State<addressList> {

  Future<AddressListResponse>_AddresList;

 String id;
  getDataFromSharedPrefs() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      id = sharedPreferences.getString("tokenCall");
      print("welcome to AddressList Ravan page ravan${id}");
      _AddresList=ApiCall.fetchaddressListResponse(id);
      // firstName = sharedPreferences.getString("firstName");
    });
  }

  @override
  void initState() {
    getDataFromSharedPrefs();

   // print("Hello ravan your id you are genuis  and u hav the right to live in this world and your id is  ${id}");

    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Select the Address'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(5),
          color: Colors.grey[200],
          height: 750,
          child: Flexible(
            child: FutureBuilder(
              future:_AddresList,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    print("Snapshot has data");
                    AddressListResponse model = snapshot.data;
                    //imagedata=widget.categoryResponse.result[1].image;
                    // Map<String, String> bodyData = snapshot.data;
                    return ListView.builder(
                             itemCount: model.result.length,
                             itemBuilder: (BuildContext context,int index) {
                              return Card(
                                child: InkWell(
                                  onTap: (){
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context)=>payment(),
                                        )
                                    );
                                  },

                                  child: Container(
                                    padding: const EdgeInsets.all(10),
                                    child: Row(
                                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          padding: const EdgeInsets.all(5),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [

                                              Container(
                                                padding: const EdgeInsets.all(5),

                                                child: Text('Name : ${model.result[index].contactName}'),
                                              ),
                                              Container(
                                                padding: const EdgeInsets.all(5),
                                                child: Text('Address : ${model.result[index].address1.toString()}'),
                                              ),


                                            ],
                                          ),
                                        ),
                                        Container(
                                          padding: const EdgeInsets.all(5),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                padding: const EdgeInsets.all(5),
                                                child: Text('Mobile : ${model.result[index].contactNumber}'),
                                              ),
                                              Container(
                                                padding: const EdgeInsets.all(5),
                                                child: Text('Type : ${model.result[index].lebel.toString()}'),
                                              ),
                                              Container(
                                                padding: const EdgeInsets.all(5),
                                                child: Text('PinCode : ${model.result[index].pincode}'),
                                              ),
                                            ],
                                          ),
                                        ),

                                      ],
                                    ),
                                  ),
                                ),

                              );
                      }
                    );
                  }
                  else{
                    print("ERROR.${snapshot.error}");
                    return Center(
                        child: Column(
                          children: [
                            CircularProgressIndicator(),
                            //Text("Error.${snapshot.error}")

                          ],
                        ));
                  }
                }
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: (){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => cheakout()),
            );

        },
         child: Icon(Icons.add),
        backgroundColor: Colors.indigo,
      ),
    );

  }

}
