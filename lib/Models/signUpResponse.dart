import 'dart:convert';

SignUpResponse signUpResponseFromJson(String str) => SignUpResponse.fromJson(json.decode(str));

String signUpResponseToJson(SignUpResponse data) => json.encode(data.toJson());

class SignUpResponse {
  SignUpResponse({
    this.status,
    this.otp,
    this.message,
    this.pincode,
    this.mobile,
    this.email,
    this.address,
    this.village,
    this.postOffice,
    this.thana,
    this.state,
    this.district,

  });
  String status;
  String message;
  int otp;
  String pincode;
  String mobile;
  String email;
  String address;
  String village;
  String postOffice;
  String thana;
  String state;
  String district;

  factory SignUpResponse.fromJson(Map<String, dynamic> json) => SignUpResponse(
    status: json["status"],
    message: json["message"],
    otp: json["OTP"],
    pincode: json["pincode"],
    mobile: json["mobile"],
    email:  json["email"],
    address: json["address"],
    village: json["village"],
    postOffice: json["post_office"],
    thana: json["thana"],
    state: json["state"],
    district: json["district"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "message": message,
    "OTP": otp,
    "pincode": pincode,
    "mobile": mobile,
    "email" :email,
    "address": address,
    "village": village,
    "post_office": postOffice,
    "thana": thana,
    "state": state,
    "district": district,
  };
}
