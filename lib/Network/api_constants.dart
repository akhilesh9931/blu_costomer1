class AppConstants {
  static final String baseUrl = 'http://139.59.75.40:4040/app/';




  static final String home = baseUrl + 'customer-home';
  static final String otp = baseUrl +'send-otp-customer';
  static final String otpverfiy =baseUrl +'verify-otp-customer';
  static final String signup = baseUrl + 'customer-register';

  static final String category = baseUrl + 'customer-category';
  static final String categoryProduct = baseUrl + 'category-product';
  static final String customerSubCategory =baseUrl +'customer-sub-category';
  static final String subCategory =baseUrl +'sub-category-product';
  static final String productDeatail =baseUrl +'product-details';

  static final String editProfile =baseUrl +'update-customer';
  static final String addAddress =baseUrl +'add-address';
  static final String addressList =baseUrl +'address-list';

  static final String stateDist =baseUrl +'state-district';

// this is for the profile







}
