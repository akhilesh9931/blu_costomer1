import 'package:blu_customer/screen/editprofile.dart';
import 'package:blu_customer/screen/login.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:blu_customer/screen/oder.dart';
import 'package:shared_preferences/shared_preferences.dart';
class Mydrawer extends StatefulWidget {

  // const Mydrawer( this. mobile,   {Key key,}) : super(key: key);

  @override
  _MydrawerState createState() => _MydrawerState();
}

class _MydrawerState extends State<Mydrawer> {

  String id;
   _signOut(){
     Navigator.push(
         context,
         MaterialPageRoute(
           builder: (context)=>Login(),
         )
     );

  }

  @override
  void initState() {
    //id=widget.mobile;
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        height: 200,
        color: Colors.indigo[900],
        child: ListView(
          //padding: const EdgeInsets.all(10),
          children: [
            Container(
              child: DrawerHeader(
                child:Row(
                  children: [
                    Container(
                     // child: Image.asset('assets/banner.png'),
                      //padding: const EdgeInsets.only(bottom: 20),
                      margin: const EdgeInsets.only(top: 10,bottom: 10),
                      height: 32,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.redAccent,
                        image: DecorationImage(
                          image:AssetImage('assets/banner.png'),
                          // image: NetworkImage("ADD IMAGE URL HERE"),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Container(
                      child: Text("Jhon smit",style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.bold,
                          color: Colors.white
                      ),),
                    ),
                  ],
                ),
              //  decoration: BoxDecoration(color: Colors.redAccent),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 0),
              height: 490,
              child: Column(
                children: [
                  Container(
                    child: InkWell(
                      onTap: (){
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context)=>editprofile(),
                            )
                        );

                      },

                      child: ListTile(
                        title: Text("Edit Profile",style: TextStyle(
                          color: Colors.white,
                          fontSize: 22
                        ),),
                        trailing: Icon(Icons.arrow_forward_ios,color: Colors.white,size: 26,),
                      ),
                    ),
                  ),
                  // ListTile(
                  //   title: Text("My wishlist",style: TextStyle(
                  //       color: Colors.white,
                  //       fontSize: 22
                  //   ),),
                  //   trailing: Icon(Icons.arrow_forward_ios,color: Colors.white,size: 26,),
                  // ),
                  ListTile(
                    title: Text("My Oder",style: TextStyle(
                        color: Colors.white,
                        fontSize: 22
                    ),),
                    trailing: Icon(Icons.arrow_forward_ios,color: Colors.white,size: 26,),
                    onTap: (){
                      Navigator.of(context).pushReplacementNamed(Order.routeName);
                    },
                  ),
                  ListTile(
                    title: Text("Rate our App",style: TextStyle(
                        color: Colors.white,
                        fontSize: 22
                    ),),
                    trailing: Icon(Icons.arrow_forward_ios,color: Colors.white,size: 26,),
                  ),
                  InkWell(
                    onTap: () async {
                         SharedPreferences sharedPreferences=await SharedPreferences.getInstance();
                         sharedPreferences.remove('user_otp');
                         _signOut();

                      },


                    child: ListTile(
                      title: Text("Logout",style: TextStyle(
                          color: Colors.white,
                          fontSize: 22
                      ),),
                      trailing: Icon(Icons.arrow_forward_ios,color: Colors.white,size: 26,),
                    ),
                  ),

                ],
              ),
            ),

          ],
        ),
      ),

    );
  }
}

