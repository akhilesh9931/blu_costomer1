import 'package:blu_customer/Models/signUpResponse.dart';
import 'package:blu_customer/screen/signupOtp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:blu_customer/screen/login.dart';
import 'package:blu_customer/Network/api_call.dart';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:io' as Io;

class signup extends StatefulWidget {
  @override
  _signupState createState() => _signupState();
}

class _signupState extends State<signup> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneNumberController = TextEditingController();

  final _form = GlobalKey<FormState>();//for storing form state.
  SignUpResponse _user;
//saving form after validation
  Future<void> _saveForm() async {
    final isValid = _form.currentState.validate();
    if (!isValid) {
      return;
    }
    else{
      SignUpResponse signUpResponse = await ApiCall.postSignUp(
          email: emailController.text,
          mobile: phoneNumberController.text..toString(),
      );
      if (signUpResponse == null) {
        print('data not  sent or error');
      }
      else{
        if (signUpResponse.status == "success") {
          print(signUpResponse.status);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => signupOtp(otpData)),
          );
          setState(() {
            isLoading = false;
            otpData = signUpResponse.otp
                .toString();
            print(otpData);
            response =
                signUpResponse.status;
            // print("signupResponse of it is:"+ signUpResponse)
          });
        }



      }
      // print(emailController.text);
      // print( phoneNumberController.text);
    }
  }



  List signUpData;
  String response;
  bool isLoading;
  String otpData, statusData, message, mobile, tokenCall;
  String number;
  String email;

  @override
  // void initState(){
  //   super.initState();
  //   mobile = '';
  //   email = '';
  //
  // }



  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(top: 70),
          color: Colors.white,
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.only(right: 60),
                // color: Colors.blue,
                width: 500,
                height: 200,

                alignment: Alignment.center,
                child: Image.asset("assets/loginlogo.png"),

              ),
              Container(
                // color: Colors.yellow,
                  width: 500,
                  height: 50,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        // padding: const EdgeInsets.all(10),
                        width: 120,
                        height: 40,
                        //color: Colors.red,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(22.0),
                              side: BorderSide(color: Colors.black)),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => Login()),
                            );
                          },
                          color: Colors.white,
                          textColor: Colors.black,
                          child: Text("LOGIN",
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold
                            ),),

                        ),

                      ),
                      Container(
                        // padding: const EdgeInsets.all(10),
                        width: 120,
                        height: 40,
                        //color: Colors.red,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(22.0),
                              side: BorderSide(color: Colors.black)),
                          onPressed: () {},
                          color: Colors.white,
                          textColor: Colors.black,
                          child: Text("SIGN UP",
                            style: TextStyle(
                                fontSize: 18,
                                color: Colors.black,
                                fontWeight: FontWeight.bold
                            ),),

                        ),

                      ),


                    ],
                  )
              ),

              Container(
                  padding: const EdgeInsets.only(top: 10,right: 20,left: 20),

                  //color: Colors.green,
                  width: 500,
                  height: 290,
                  child: Form(
                    key: _form,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          // padding: const EdgeInsets.all(20),
                          child: TextFormField(
                            controller: emailController,
                            autocorrect: true,
                            validator: (text) {
                              if (!(text.contains('@')) && text.isNotEmpty) {
                                return "Enter a valid email address!";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: "Email",
                              prefixIcon: Icon(Icons.email),
                              hintStyle: TextStyle(color: Colors.grey),
                              filled: true,
                               fillColor: Colors.white70,
                               enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.black, width: 1),

                              ),
                            ),

                          ),
                        ),
                        Container(
                          // padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            controller: phoneNumberController,
                            autocorrect: true,
                            validator: (text) {
                              if (!(text.length == 10) && text.isNotEmpty) {
                                return "Enter valid 10 digit mobile number";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: "Mobile Number",
                              prefixIcon: Icon(Icons.lock),
                              hintStyle: TextStyle(color: Colors.grey),
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.black, width: 1),

                              ),
                            ),

                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 10),
                          width: 500,
                          height: 60,
                          //color: Colors.red,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                side: BorderSide(color: Colors.black)),
                            onPressed: () => _saveForm(),

                            color: Colors.indigo[800],
                            textColor: Colors.black,
                            child: Text("GET OTP",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold
                              ),),

                          ),

                        ),




                      ],
                    ),
                  )
              ),
              Container(
                //padding: const EdgeInsets.only(top: 70),
                //color: Colors.blue,
                alignment: Alignment.bottomRight,
                // width: 500,
                //height: 300,
                child: Image.asset("assets/wave.png"),
              )

            ],
          ),
        ),
      ),
    );
  }
}

