import 'package:blu_customer/Models/cartResponse.dart';
import 'package:blu_customer/screen/confirmation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
//enum Payment{Cashdelivery,netbanking}
enum SingingCharacter { lafayette, jefferson }


class payment extends StatefulWidget {
  // payment(String id);

  @override
  _paymentState createState() => _paymentState();
}

class _paymentState extends State<payment> {
 // Payment _pay = Payment.Cashdelivery;
  SingingCharacter _character = SingingCharacter.lafayette;



  @override
  Widget build(BuildContext context) {
    final cart=Provider.of<Cart>(context,);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.pop(context,true);
            }
        ),
        backgroundColor:Colors.indigo[800],
        title: Container(
            padding: const EdgeInsets.only(top: 10,bottom: 5),
            child: Text('Checkout', style: TextStyle(fontSize: 28.0,fontWeight: FontWeight.bold),)),
        actions: <Widget>[
          IconButton(
            iconSize: 35,
            icon: const Icon(Icons.notifications),
            tooltip: 'Show Snackbar',
            onPressed: () {

            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Container(
                // color: Colors.red,width: 350,
                //height: 50,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text("Shipping",style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.indigo

                      ),),
                      decoration:BoxDecoration(

                          border: Border(
                              bottom: BorderSide(width: 3,color: Colors.green)
                          )
                      ) ,
                      padding: EdgeInsets.all(16),
                    ),
                    Container(
                      child: Text("Payment",style: TextStyle(
                          fontSize: 18,
                          color: Colors.indigo,
                        fontWeight: FontWeight.bold
                      ),),
                      decoration:BoxDecoration(
                          border: Border(
                              bottom: BorderSide(width: 3,color: Colors.green)
                          )
                      ),
                      padding: EdgeInsets.all(16),
                    ),
                    Container(
                      child: Text("Confirmation",style: TextStyle(
                          fontSize: 18,
                          color: Colors.grey
                      ),),
                      decoration:BoxDecoration(
                          border: Border(
                              bottom: BorderSide(width: 3,color: Colors.grey)
                          )
                      ),
                      padding: EdgeInsets.all(16),
                    )
                  ],
                ),
              ),
              //this is for the prome code
              Container(
                height: 125,
                color: Colors.grey[200],
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      //color: Colors.red,
                      height: 50,
                      width: 120,
                      child: Text('Promo code',style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500
                      ),),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            margin: const EdgeInsets.only(left: 10),
                            width: 220,
                            height: 50,
                            child: TextField(
                              autocorrect: true,
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                hintText: '#1234',
                                hintStyle: TextStyle(color: Colors.grey),
                                filled: true,
                                fillColor: Colors.white,
                              ),
                            ),
                          ),

                          Container(
                              color: Colors.green,
                            child: RaisedButton(
                              color: Colors.green,
                              onPressed: () {},
                              textColor: Colors.white,
                              padding: const EdgeInsets.all(2.0),
                              child: Container(
                                decoration: const BoxDecoration(

                                  // gradient: LinearGradient(
                                  //   colors: <Color>[
                                  //     Color(0xFF0D47A1),
                                  //     Color(0xFF1976D2),
                                  //     Color(0xFF42A5F5),
                                  //   ],
                                  // ),
                                ),
                                padding: const EdgeInsets.all(10.0),
                                child:
                                const Text('APPLY', style: TextStyle(fontSize: 20)),
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),
                  ],
                ),
              ),

              //this is for cash on delivery
              Container(
                height: 100,
                margin: const EdgeInsets.only(left: 5,right: 5),
                child: Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 10),
                      child: Image.asset('assets/cash.png',fit: BoxFit.fill,color: Colors.black,),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 30),
                      child: Text('Cash in delivery',style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 25
                      ),),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 60,right: 5),
                      width: 10,
                      child: ListTile(
                        contentPadding: const EdgeInsets.all(5),
                        leading:
                        Radio(
                          value: SingingCharacter.lafayette,
                          groupValue: _character,
                            onChanged: (SingingCharacter value){
                              setState(() {
                                _character = value;
                              });

                            }
                          // value: Payment.Cashdelivery,
                          // groupValue: _pay,
                          // onChanged: ( Payment value) {
                          //   setState(() {
                          //     _pay = value;
                          //
                          //   });
                          // },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 5,right: 5),
                height: 100,
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(width: 1.0,color: Colors.grey,)
                  )
                ),
                child: Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(left: 10),
                      child: Image.asset('assets/net-banking.png',fit: BoxFit.fill,color: Colors.black,),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 50),
                      child: Text('Net banking',style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 25
                      ),),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 84,right: 5),
                      width: 10,
                      child: ListTile(
                        contentPadding: const EdgeInsets.all(5),
                        leading: Radio(
                        value: SingingCharacter.jefferson,
                        groupValue: _character,
                        onChanged: (SingingCharacter value) {
                          setState(() {
                            _character = value;
                          });
                      
                    }
                          // value: Payment.netbanking,
                          // groupValue: _pay,
                          // onChanged: (Payment value) {
                          //   setState(() {
                          //     _pay = value;
                          //
                          //   });
                          // },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        top: BorderSide(width: 0.5,color: Colors.grey,)
                    )
                ),
                margin: const EdgeInsets.only(top: 220),
                padding: const EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text('Total Amount',style: TextStyle(
                    fontSize: 18,
                ),),
                    ),
                    Container(
                      child: Text(cart.totalAmount.toString(),style: TextStyle(
                        color: Colors.green,
                        fontSize: 25
                      ),),
                    )
                  ],
                ),
              ),
              Container(
                alignment: Alignment.center,
                height: 60,
                width: 400,
                //margin: const EdgeInsets.only(bottom: 25,),
                padding:  const EdgeInsets.all(10),
                decoration: BoxDecoration(
                    color: Colors.indigo[800],
                    borderRadius:  BorderRadius.circular(1)
                ),
                child: InkWell(
                  onTap: (){
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context)=>confirmation(),
                        )
                    );

                  },
                  child: Text("CONTINUE TO CONFIRMATION",style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Colors.white
                  ),),
                ),
              ),
            ],
          )
        ),
      ),
    );
  }
}
