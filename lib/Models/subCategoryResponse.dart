// To parse this JSON data, do
//
//     final subCategoryResponse = subCategoryResponseFromJson(jsonString);

import 'dart:convert';

SubCategoryResponse subCategoryResponseFromJson(String str) => SubCategoryResponse.fromJson(json.decode(str));

String subCategoryResponseToJson(SubCategoryResponse data) => json.encode(data.toJson());

class SubCategoryResponse {
  SubCategoryResponse({
    this.status,
    this.token,
    this.categoryBaseUrl,
    this.result,
  });

  String status;
  String token;
  String categoryBaseUrl;
  List<Result> result;

  factory SubCategoryResponse.fromJson(Map<String, dynamic> json) => SubCategoryResponse(
    status: json["status"],
    token: json["token"],
    categoryBaseUrl: json["category_base_url"],
    result: List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "token": token,
    "category_base_url": categoryBaseUrl,
    "result": List<dynamic>.from(result.map((x) => x.toJson())),
  };
}

class Result {
  Result({
    this.parentGroup,
    this.childGroup,
    this.brand,
    this.unit,
    this.state,
    this.active,
    this.qty,
    this.deleted,
    this.id,
    this.name,
    this.unitValue,
    this.vendor,
    this.manufacturingPrice,
    this.sellingPriceOrg,
    this.sellingPrice,
    this.cgst,
    this.sgst,
    this.igst,
    this.batchCode,
    this.totalProductPack,
    this.totalPackBox,
    this.boxHeight,
    this.boxLength,
    this.boxWidth,
    this.city,
    this.image,
    this.createdDate,
    this.v,
  });

  String parentGroup;
  String childGroup;
  String brand;
  String unit;
  String state;
  int active;
  int qty;
  int deleted;
  String id;
  String name;
  String unitValue;
  String vendor;
  String manufacturingPrice;
  String sellingPriceOrg;
  String sellingPrice;
  String cgst;
  String sgst;
  String igst;
  String batchCode;
  String totalProductPack;
  String totalPackBox;
  String boxHeight;
  String boxLength;
  String boxWidth;
  String city;
  String image;
  DateTime createdDate;
  int v;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
    parentGroup: json["parent_group"],
    childGroup: json["child_group"],
    brand: json["brand"],
    unit: json["unit"],
    state: json["state"],
    active: json["active"],
    qty: json["qty"],
    deleted: json["deleted"],
    id: json["_id"],
    name: json["name"],
    unitValue: json["unit_value"],
    vendor: json["vendor"],
    manufacturingPrice: json["manufacturing_price"],
    sellingPriceOrg: json["selling_price_org"],
    sellingPrice: json["selling_price"],
    cgst: json["cgst"],
    sgst: json["sgst"],
    igst: json["igst"],
    batchCode: json["batch_code"],
    totalProductPack: json["total_product_pack"],
    totalPackBox: json["total_pack_box"],
    boxHeight: json["box_height"],
    boxLength: json["box_length"],
    boxWidth: json["box_width"],
    city: json["city"],
    image: json["image"],
    createdDate: DateTime.parse(json["created_date"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "parent_group": parentGroup,
    "child_group": childGroup,
    "brand": brand,
    "unit": unit,
    "state": state,
    "active": active,
    "qty": qty,
    "deleted": deleted,
    "_id": id,
    "name": name,
    "unit_value": unitValue,
    "vendor": vendor,
    "manufacturing_price": manufacturingPrice,
    "selling_price_org": sellingPriceOrg,
    "selling_price": sellingPrice,
    "cgst": cgst,
    "sgst": sgst,
    "igst": igst,
    "batch_code": batchCode,
    "total_product_pack": totalProductPack,
    "total_pack_box": totalPackBox,
    "box_height": boxHeight,
    "box_length": boxLength,
    "box_width": boxWidth,
    "city": city,
    "image": image,
    "created_date": createdDate.toIso8601String(),
    "__v": v,
  };
}
