import 'package:blu_customer/Models/otpverify.dart';
import 'package:blu_customer/Models/profileResponse.dart';
import 'package:blu_customer/Network/api_call.dart';
import 'package:blu_customer/screen/BottomNavigationBar.dart';
import 'package:blu_customer/screen/editprofile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';


class profile extends StatefulWidget {

  final VerifyOtpResponse verifyOtpResponse;
  const profile({Key key, this.verifyOtpResponse}) : super(key: key);
  @override
  _profileState createState() => _profileState();
}

class _profileState extends State<profile> {
  Future<SharedPreferences> prefs;
  String Mobile;
  String profileImage;
  String mobile;
  String address;
  String village;
  String state;
  String district;
  String postOffice;
  String thana;
  String pincode;
  String name;
  String id;
  String otp;
  bool isLoading;
  String email;
  String response;


  getDataFromSharedPrefs() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      id = sharedPreferences.getString("tokenCall");
      name = sharedPreferences.getString("fullName");
      email = sharedPreferences.getString("email");
      mobile = sharedPreferences.getString("mobile");
      address= sharedPreferences.getString("address");
      pincode= sharedPreferences.getString("pincode");
      state= sharedPreferences.getString("state");
      district= sharedPreferences.getString("dist");
      print(id);
      print(name);
      print(email);
      print(mobile);
      // firstName = sharedPreferences.getString("firstName");
    });
  }


  @override
  void initState() {
    prefs = SharedPreferences.getInstance();

     getDataFromSharedPrefs();


    super.initState();
  }
  circularProgress(){
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MyNavigationBar()),
              );
            }
        ),
        backgroundColor:Colors.indigo[800],
        title: Container(
            padding: const EdgeInsets.only(top: 10,bottom: 5),
            child: Text('Profile', style: TextStyle(fontSize: 28.0,fontWeight: FontWeight.bold),)),

      ),
      body: SingleChildScrollView(

        child:FutureBuilder(
          future: prefs,
          builder: (context, AsyncSnapshot<SharedPreferences> snapshot) {
            if (!snapshot.hasData) {
              return circularProgress();
            }
            else{
              return Container(

                height: 600,
                color: Colors.white,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 200,
                      color: Colors.indigo[800],
                      child: Column(
                        children: [
                          Container(
//padding: const EdgeInsets.only(bottom: 20),
                            margin: const EdgeInsets.only(top: 10,bottom: 10),
                            height: 125,
                            decoration: BoxDecoration(
//borderRadius: BorderRadius.circular(50),
                              shape: BoxShape.circle,
                              color: Colors.redAccent,
                              image: DecorationImage(
                                image:AssetImage('assets/banner.png'),
// image: NetworkImage("ADD IMAGE URL HERE"),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
//                           Container(
// //model.name
//                             child: Text(id,style: TextStyle(
//                                 fontSize: 25,
//                                 fontWeight: FontWeight.bold,
//                                 color: Colors.white
//                             ),),
//                           ),

                        ],
                      ),
                    ),

                    Container(
                      padding: const EdgeInsets.all(30),
                      child: Column(

                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                        child: Text("First name",style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 18
                                        ),),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                        child: Text(name,style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18
                                        ),),
                                      ),

                                    ],
                                  ),
                                ),

                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              children: [
                                Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                        child: Text("Email",style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 18
                                        ),),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                        child: Text(email,style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18
                                        ),),
                                      ),

                                    ],
                                  ),
                                ),

                              ],
                            ),
                          ), //t
                          Container(

                            alignment: Alignment.bottomLeft,
                            padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                            child: Text("Address",style: TextStyle(
                                color: Colors.grey,
                                fontSize: 18
                            ),),
                          ),
                          Container(
// margin: const EdgeInsets.only(top: 10,) ,
                            alignment: Alignment.bottomLeft,
                            padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                            child: Text(address,style: TextStyle(
                                color: Colors.grey,
                                fontSize: 18
                            ),),
                          ), //this is for phone number and zip code
                          Container(
                            child: Row(
                              children: [
                                Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                        child: Text("Phone number",style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 18
                                        ),),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                        child: Text(mobile,style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18
                                        ),),
                                      ),

                                    ],
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                        child: Text("Pin Code",style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 18
                                        ),),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                        child: Text(pincode,style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18
                                        ),),
                                      ),

                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              children: [
                                Container(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                        child: Text("City",style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 18
                                        ),),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                        child: Text(district,style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18
                                        ),),
                                      ),

                                    ],
                                  ),
                                ),
                                Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                        child: Text("State",style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 18
                                        ),),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                        child: Text(state,style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18
                                        ),),
                                      ),

                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),

//this is for the save button

                    // Container(
                    //   alignment: Alignment.center,
                    //   width: 330,
                    //   height: 50,
                    //   margin: const EdgeInsets.only(bottom: 25,top: 60),
                    //   padding:  const EdgeInsets.all(10),
                    //   decoration: BoxDecoration(
                    //       color: Colors.indigo[800],
                    //       borderRadius:  BorderRadius.circular(30)
                    //   ),
                    //   child: InkWell(
                    //     onTap: (){
                    //       Navigator.push(
                    //           context,
                    //           MaterialPageRoute(
                    //             builder: (context)=>editprofile(),
                    //           )
                    //       );
                    //
                    //     },
                    //     child: Text("Update",style: TextStyle(
                    //         fontSize: 25,
                    //         fontWeight: FontWeight.bold,
                    //         color: Colors.white
                    //     ),),
                    //   ),
                    // ),
                  ],

                ),
              );


          }

          }
        )
      ),
    );
  }
}


