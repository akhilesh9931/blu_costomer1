import 'package:blu_customer/Models/order.dart';
import 'package:blu_customer/screen/BottomNavigationBar.dart';
import 'package:blu_customer/screen/oder_items.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class Order extends StatelessWidget {
  static const routeName ='/orders';
  @override
  Widget build(BuildContext context) {
    final ordersData=Provider.of<Orders>(context);
    return Scaffold(
      appBar: AppBar(
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MyNavigationBar()),
              );
            }
        ),
        title: Container(
          alignment: Alignment.center,
           // margin: const EdgeInsets.only(left: 80),
           // padding: const EdgeInsets.only(top: 10,bottom: 5),
            child: Text('My order', style: TextStyle(fontSize: 28.0,fontWeight: FontWeight.bold),)
        ),
      ),
      body: ListView.builder(
        itemCount: ordersData.orders.length,
          itemBuilder: (ctx,i)=>OrderItemScreen(
            ordersData.orders[i]
          )
      ),
    );
  }
}
