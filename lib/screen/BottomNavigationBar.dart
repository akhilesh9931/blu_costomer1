import 'package:blu_customer/Models/categoryResponse.dart';
import 'package:blu_customer/Models/homeResponse.dart';
import 'package:blu_customer/Models/otpverify.dart';
import 'package:blu_customer/screen/cart.dart';
import 'package:blu_customer/Models/homeResponse.dart';
import 'package:blu_customer/screen/profile.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:blu_customer/screen/checkout.dart';
import 'package:blu_customer/screen/getcategory.dart';
import 'package:blu_customer/screen/drawer.dart';
import 'package:blu_customer/screen/editprofile.dart';
import 'package:blu_customer/screen/homepage.dart';
import 'package:blu_customer/screen/category.dart';
import 'package:blu_customer/screen/login.dart';
import 'package:blu_customer/screen/order.dart';
import 'package:blu_customer/screen/product_detail.dart';
import 'package:dio/dio.dart';
import 'package:blu_customer/screen/signup.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MyNavigationBar extends StatefulWidget {
  final HomeResponse homeResponse;
  final CategoryResponse categoryResponse;
  final VerifyOtpResponse verifyOtpResponse;
  //final String mobile;



  const MyNavigationBar(   {Key key,
  // this.mobile,
    this.homeResponse,
    this.categoryResponse,
    this.verifyOtpResponse,
  }) : super(key: key);

  //MyNavigationBar (this.homeResponse,);


  @override
  _MyNavigationBarState createState() => _MyNavigationBarState();
}

class _MyNavigationBarState extends State<MyNavigationBar > {
  String id;

  @override
  void initState() {

    super.initState();
  }

  int selectedPage = 0;
  final _pageOptions = [

    home(),
   // order(),
    cart(),
    category(),
    profile(),

  ];




  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white54,
        body: _pageOptions[selectedPage],
        bottomNavigationBar: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
                icon:
                    ImageIcon(AssetImage("assets/home.png"),
                      size: 32, color: Colors.black,),
                 title: Text('Home')
            ),


            //BottomNavigationBarItem(icon: ImageIcon(AssetImage("assets/off.png"), size: 32,color: Colors.black,), title: Text('Hot offer')),
            BottomNavigationBarItem(icon: ImageIcon(AssetImage("assets/cart.png"), size: 32,color: Colors.black,), title: Text('My Cart')),
            BottomNavigationBarItem(icon: ImageIcon(AssetImage("assets/category.png"),size: 32, color: Colors.black,), title: Text('Category')),
            BottomNavigationBarItem(icon: ImageIcon(AssetImage("assets/user.png"), size: 32,color: Colors.black,), title: Text('Profile')),


          ],
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Colors.indigo[900],
          elevation: 5.0,
          unselectedItemColor: Colors.black,
          currentIndex: selectedPage,
          backgroundColor: Colors.white,
          onTap: (index){
            setState(() {
              selectedPage = index;
            });
          },
        )
    );
  }
}
