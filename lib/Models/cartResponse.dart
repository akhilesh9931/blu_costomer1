import 'package:flutter/foundation.dart';

class CartItem {
   final String id;
   final String image;
   final String name;
   final String size;
   final int quantity;
   final double price;

  CartItem({
        @required this.id,
        @required this.name,
        @required this.image,
         this.size,
         this.quantity,
        @required this.price
      });
}

class Cart with ChangeNotifier {
  Map<String, CartItem> _items ={};

  Map<String, CartItem> get items {
    return {..._items};
  }

  int get itemCount {
    return _items.length;
  }
  double get totalAmount{
    var total =0.0;
    _items.forEach((key, cartItem) {
      total +=cartItem.price*cartItem.quantity;
     // total +=cartItem.price *cartItem.quantity;

    });
    return total;
  }

  void addItem(
      String productId,
      String name,
      double price,
      String image,
      int quantity
      ) {
    if (_items.containsKey(productId)) {
      _items.update(
          productId,
              (existingCartItem) => CartItem(
              id: DateTime.now().toString(),
              name: existingCartItem.name,
              image:existingCartItem.image,
              size: existingCartItem.size,
              quantity: existingCartItem.quantity + 1,
              price: existingCartItem.price));
    } else {
      _items.putIfAbsent(
          productId,
              () => CartItem(
            name: name,
            id: DateTime.now().toString(),
            quantity: 1,
            price: price,
            image: image,
          ));
    }
    notifyListeners();
  }

  void removeItem(String productId) {
    _items.remove(productId);
    notifyListeners();
  }

  void removeSingleItem(String productId) {
    if (!_items.containsKey(productId)) {
      return;
    }
    if (_items[productId].quantity > 1) {
      _items.update(
          productId,
              (existingCartItem) => CartItem(
              id: DateTime.now().toString(),
              name: existingCartItem.name,
              image: existingCartItem.image,
              size:existingCartItem.size,
              quantity: existingCartItem.quantity - 1,
              price: existingCartItem.price));
    }
    notifyListeners();
  }

  // double get totalAmount {
  //   var total = 0.0;
  //   _items.forEach((key, cartItem) {
  //     total += cartItem.price * cartItem.quantity;
  //   });
  //   return total;
  // }

  void clear() {
    _items = {};
    notifyListeners();
  }

}