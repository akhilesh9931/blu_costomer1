import 'package:blu_customer/screen/Payment.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';


class shipping extends StatefulWidget {
  @override
  _shippingState createState() => _shippingState();
}

class _shippingState extends State<shipping> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
       // color: Colors.red,
        child: Column(
          children: [
            Container(
              //height: 500,
              child: Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 40),
                    child: Row(
                      children: [
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                child: Text("First name",style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 18
                                ),),
                              ),
                              Container(
                                //color: Colors.redAccent,
                                height: 35,
                                width: 180,
                                padding: const EdgeInsets.only(left: 15,right: 10,bottom: 10),
                                child: TextField(
                                  autocorrect: true,
                                  decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 2,
                                            color: Colors.grey
                                        ),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color:Colors.black)
                                      )
                                  ),

                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                child: Text("Last name",style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 18
                                ),),
                              ),
                              Container(
                                //color: Colors.redAccent,
                                height: 35,
                                width: 180,
                                padding: const EdgeInsets.only(left: 15,right: 10,bottom: 10),
                                child: TextField(
                                  autocorrect: true,
                                  decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 2,
                                            color: Colors.grey
                                        ),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color:Colors.black)
                                      )
                                  ),

                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),

                  //this is for address
                  Container(
                    // margin: const EdgeInsets.only(top: 10,) ,
                    alignment: Alignment.bottomLeft,
                    padding: const EdgeInsets.only(left: 15,top: 15,right: 15,) ,
                    child: Text("Address",style: TextStyle(
                        color: Colors.grey,
                        fontSize: 18
                    ),),
                  ),
                  Container(
                    alignment: Alignment.bottomLeft,
                    //color: Colors.redAccent,
                    height: 35,
                    width: 360,
                    padding: const EdgeInsets.only(right: 10,bottom: 10),
                    child: TextField(
                      autocorrect: true,
                      decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                width: 2,
                                color: Colors.grey
                            ),
                          ),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color:Colors.black)
                          )
                      ),

                    ),
                  ),
                  //this is for phone number and zip code
                  Container(
                    child: Row(
                      children: [
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                child: Text("Phone number",style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 18
                                ),),
                              ),
                              Container(
                                //color: Colors.redAccent,
                                height: 35,
                                width: 180,
                                padding: const EdgeInsets.only(left: 15,right: 10,bottom: 10),
                                child: TextField(
                                  autocorrect: true,
                                  decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 2,
                                            color: Colors.grey
                                        ),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color:Colors.black)
                                      )
                                  ),

                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                child: Text("Zip Code",style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 18
                                ),),
                              ),
                              Container(
                                //color: Colors.redAccent,
                                height: 35,
                                width: 180,
                                padding: const EdgeInsets.only(left: 15,right: 10,bottom: 10),
                                child: TextField(
                                  autocorrect: true,
                                  decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 2,
                                            color: Colors.grey
                                        ),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color:Colors.black)
                                      )
                                  ),

                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: [
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                child: Text("City",style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 18
                                ),),
                              ),
                              Container(
                                //color: Colors.redAccent,
                                height: 35,
                                width: 180,
                                padding: const EdgeInsets.only(left: 15,right: 10,bottom: 10),
                                child: TextField(
                                  autocorrect: true,
                                  decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 2,
                                            color: Colors.grey
                                        ),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color:Colors.black)
                                      )
                                  ),

                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                                child: Text("State",style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 18
                                ),),
                              ),
                              Container(
                                //color: Colors.redAccent,
                                height: 35,
                                width: 180,
                                padding: const EdgeInsets.only(left: 15,right: 10,bottom: 10),
                                child: TextField(
                                  autocorrect: true,
                                  decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                            width: 2,
                                            color: Colors.grey
                                        ),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(color:Colors.black)
                                      )
                                  ),

                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),


            //this is for the save button

            Container(

              alignment: Alignment.center,
              width: 400,
              height: 50,
             margin: const EdgeInsets.only(top:280),
              padding:  const EdgeInsets.all(10),
              decoration: BoxDecoration(

                  color: Colors.indigo[800],
                  borderRadius:  BorderRadius.circular(1)
              ),
              child: InkWell(
                // onTap: (){
                //   Navigator.push(
                //       context,
                //       MaterialPageRoute(
                //         builder: (context)=>payment(),
                //       )
                //   );
                //
                // },


                child: Text("CONTINUE TO PAYMENT",style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.white
                ),),
              ),
            ),
          ],
        ),
      ),

    );
  }
}
