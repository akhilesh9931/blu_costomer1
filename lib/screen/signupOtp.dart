import 'package:blu_customer/Models/otpverify.dart';
import 'package:blu_customer/Models/signUpResponse.dart';
import 'package:blu_customer/Network/api_call.dart';
import 'package:blu_customer/screen/login.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:blu_customer/screen/signup.dart';

class signupOtp extends StatefulWidget {
  final String otpData;
  signupOtp(this.otpData, {Key key}): super(key: key);


  @override
  _signupOtpState createState() => _signupOtpState();
}

class _signupOtpState extends State<signupOtp> {

  final TextEditingController signUpOtp = TextEditingController();
  final _form = GlobalKey<FormState>();



  Future<void> _VerifyOtp() async {
    final isValid = _form.currentState.validate();
    if (!isValid) {
      return;
    }
    else{
      if (widget.otpData==signUpOtp.text){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Login()),
        );
      }
      else{
        print('otp didnot verify');
        print(signUpOtp.text);
        print(widget.otpData);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(top: 70),
          color: Colors.white,
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.only(right: 60),
                // color: Colors.blue,
                width: 500,
                height: 200,

                alignment: Alignment.center,
                child: Image.asset("assets/loginlogo.png"),

              ),


              Container(
                  padding: const EdgeInsets.only(top: 10,right: 20,left: 20),

                  //color: Colors.green,
                  width: 500,
                  height: 390,
                  child: Form(
                    key: _form,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          // padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            controller: signUpOtp,
                            autocorrect: true,
                            validator: (text) {
                              if (!(text.length == 6) && text.isNotEmpty) {
                                return "Enter Valid 6 digit OTP No";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              hintText: "OTP",
                              prefixIcon: Icon(Icons.lock),
                              hintStyle: TextStyle(color: Colors.grey),
                              filled: true,
                              fillColor: Colors.white70,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                borderSide: BorderSide(color: Colors.black, width: 1),

                              ),
                            ),

                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 10),
                          width: 450,
                          height: 60,
                          //color: Colors.red,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                                side: BorderSide(color: Colors.black)),
                             onPressed: () =>  _VerifyOtp(),
                            color: Colors.indigo[800],
                            textColor: Colors.black,
                            child: Text("VERIFY OTP",
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold
                              ),),

                          ),

                        ),




                      ],
                    ),
                  )
              ),
              Container(
                //padding: const EdgeInsets.only(top: 70),
                //color: Colors.blue,
                alignment: Alignment.bottomRight,
                // width: 500,
                //height: 300,
                child: Image.asset("assets/wave.png"),
              )

            ],
          ),
        ),
      ),
    );
  }
}
