import 'package:flutter/material.dart';
import 'package:blu_customer/Models/order.dart';
import 'package:intl/intl.dart';
import 'dart:math';
class OrderItemScreen extends StatefulWidget {
  final OrderItems order;
  OrderItemScreen(this.order);

  @override
  _OrderItemScreenState createState() => _OrderItemScreenState();
}

class _OrderItemScreenState extends State<OrderItemScreen> {
  var _expanded =false;

  @override
  Widget build(BuildContext context) {
    return Card(
     margin: EdgeInsets.all(10),

      child:Container(
       // height: 150,
        child: Column(
          children: [
            ListTile(
              leading: Container(
              //  color: Colors.yellow,

                //width:150,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child:Text(DateFormat('#ddMMyyyyhhmmss').format(widget.order.dateTime),style: TextStyle(
                        fontSize: 18,
                        color: Colors.indigo,
                        fontWeight: FontWeight.bold,
                      ),),
                    ),
                    Container(
                      child: Text('${widget.order.amount}',style: TextStyle(
                        fontSize: 18,
                        color: Colors.black54,
                        fontWeight: FontWeight.w500,
                      ),),
                    ),
                  ],
                ),
              ),

              title: Container(

                child: Column(
                 // mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Text('${widget.order.products.length.toString()}  items',style: TextStyle(
                        fontSize: 16,
                        color: Colors.black54,
                        fontWeight: FontWeight.bold,
                      ),),
                    ),
                    Container(
                      child:  Text(DateFormat('dd /MM /yyyy').format(widget.order.dateTime),style: TextStyle(
                        fontSize: 14,
                      ),),
                    ),

                  ],
                ),
              ),
              //subtitle:
             // ),

              trailing: IconButton(
                icon: Icon(_expanded ? Icons.expand_less : Icons.expand_more),
                onPressed: (){
                setState(() {
                   _expanded =!_expanded;
                });

                },
              ),
            ),
            if(_expanded)Container(
              margin: const EdgeInsets.all(10),
              height: min(widget.order.products.length * 15.0 +100 ,180),
              child: ListView(
                children:
                  widget.order.products.map((prod)=>
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: const EdgeInsets.all(8),
                            child: Text(prod.name,style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),),
                          ),
                          Container(
                            padding: const EdgeInsets.all(8),
                            child: Text('${prod.quantity}x ${prod.price}',style: TextStyle(
                              fontSize: 17,
                              color: Colors.red,
                            ),),
                          )
                        ],
                      )

                  ).toList(),

              ),

              color: Colors.grey[100],



            )
          ],
        ),
      ) ,
    );


  }
}
