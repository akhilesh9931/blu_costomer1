import 'package:blu_customer/Models/cartResponse.dart';
import 'package:blu_customer/Models/homeResponse.dart';
import 'package:blu_customer/Network/api_call.dart';
import 'package:blu_customer/screen/badge.dart';
import 'package:blu_customer/screen/cart.dart';
import 'package:blu_customer/screen/category.dart';
import 'package:blu_customer/screen/drawer.dart';
import 'package:blu_customer/screen/homeCategory.dart';
import 'package:blu_customer/screen/product_detail.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:dio/dio.dart';

class home extends StatefulWidget {

  final HomeResponse homeResponse;
  //final String id;

  const home( {Key key, this.homeResponse}) : super(key: key);



  @override
  _homeState createState() => _homeState();
}

class _homeState extends State<home> {
  Future<HomeResponse>_future;

  String name;
  String id;
  String price;
  String detail;
  String size;
  String status;
  String img;
  Result result;





  @override
  void initState() {

   //id=widget.id;
    _future=ApiCall.fetchHomeResponse();

   // slider = widget.homeResponse.sliderBaseUrl;
   // content = widget.aboutUsResponse.content;
    super.initState();
  }
  circularProgress(){
    return Center(
      child: CircularProgressIndicator(),
    );
  }





  @override
  Widget build(BuildContext context) {
    // print('img : $slider');
    // print('status : $status');
    // print('token: $token');

    return Scaffold(
        appBar: AppBar(
          backgroundColor:Colors.indigo[900],
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SingleChildScrollView(
                child: Container(
                  width:130,

                  margin: const EdgeInsets.all(20),
                  child: Image.asset(
                    'assets/logowhite.png',
                    fit: BoxFit.contain,
                    height: 42,
                    //width: 50,
                  ),
                ),
              ),

            ],

          ),
          actions: <Widget>[
            // IconButton(
            //   iconSize: 32,
            //   icon: const Icon(Icons.notifications),
            //   tooltip: 'Show Snackbar',
            //   onPressed: () {
            //
            //   },
            // ),
            Consumer<Cart>(
                builder: (_,cart,ch)=>Badge(
                  child:ch,
                   value: cart.itemCount.toString(),
                ),
              child: IconButton(
                iconSize: 32,
                icon: const Icon(Icons.shopping_cart),
                tooltip: 'cart',
                onPressed: () =>
                  Navigator.of(
                      context,
                      // MaterialPageRoute(builder: (context) => cart()),
//id,name,img,price
                  ).pushNamed(cart.routeName)


              ),
            ),
          ],
        ),
      drawer: Mydrawer(),

      body: SingleChildScrollView(
        child: FutureBuilder(
          future: _future,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              print("Snapshot has data");
              HomeResponse model = snapshot.data;
              //imagedata=widget.categoryResponse.result[1].image;
              // Map<String, String> bodyData = snapshot.data;
              return Container(

                //color: Colors.red,
                //padding: const EdgeInsets.only(top: 80),
                child: Column(
                  children: [
                    //this section is for the slider
                    Container(
                      padding: const EdgeInsets.all(20),
                      color: Colors.indigo[900],
                      height: 200,
                      child: ListView(
                        children: [
                          CarouselSlider.builder(
                            itemCount: model.result.sliderImages.length,
                            itemBuilder: (BuildContext context, int index ){
                              return Container(
                                      margin: EdgeInsets.all(6.0),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(18.0),
                                        image: DecorationImage(
                                          // image: AssetImage('assets/banner.png'),
                                          image: NetworkImage('http://139.59.75.40:4040/uploads/slider/${model.result.sliderImages[index].image}'),
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    );

                            },
                            options:  CarouselOptions(
                              height: 150.0,
                              enlargeCenterPage: true,
                              autoPlay: true,
                              aspectRatio: 16 / 9,
                              autoPlayCurve: Curves.fastOutSlowIn,
                              enableInfiniteScroll: true,
                              autoPlayAnimationDuration: Duration(milliseconds: 600),
                              viewportFraction: 1,
                            ),
                          )
                        ],
                      ),
                    ), //this is for catgory
                    Container(
                      padding: const EdgeInsets.only(top: 10),
                      height: 130,
                      color: Colors.grey[300],
                      child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: model.result.productParentGroup.length,
                        itemBuilder: (BuildContext context,int index){
                         return Container(
                           //color: Colors.red,
                              width: 110,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  InkWell(
                                    onTap: (){
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context)=>homeCategory(id=model.result.productParentGroup[index].id,name=model.result.productParentGroup[index].name),
                                          )
                                      );
                                    },
                                    child: Container(
                                      width:65,
                                      height: 65,
                                      padding: const EdgeInsets.all(7),
                                     // child: Image.network('http://139.59.75.40:4040/uploads/product_group/${model.result.productParentGroup[index].image}',fit: BoxFit.fill,),
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                            // image: AssetImage('assets/banner.png'),
                                            image: NetworkImage('http://139.59.75.40:4040/uploads/product_group/${model.result.productParentGroup[index].image}'),
                                            fit: BoxFit.fill,
                                          ),
                                         //color: Colors.indigo,
                                          borderRadius: BorderRadius.circular(12)
                                      ),
                                    ),
                                  ),
                                  Container(
                                   // color: Colors.blue,
                                    height: 40,

                                    margin:  EdgeInsets.only(top:1,left:1),
                                   // alignment: Alignment.center,
                                    padding:  EdgeInsets.all(8),
                                    child: Text(model.result.productParentGroup[index].name,
                                      style: TextStyle(fontSize: 11,color: Colors.black,fontWeight: FontWeight.bold),


                                    ),
                                  )
                                ],
                              )
                          );
                         },
                      ),
                    ),
                    Container(
                        padding: const EdgeInsets.all(10),
                        color: Colors.grey[300],
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(child: Text("PRODUCTS",style: TextStyle(color:Colors.black,fontSize: 16,fontWeight: FontWeight.bold),)),
                            Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(width: 3,color: Colors.indigo)
                                )
                              ),
                              child: InkWell(
                             // disabledColor: Colors.indigo,
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => category()),
                                  );

                                },

                                child: Text("View all",
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.indigo,
                                      fontWeight: FontWeight.bold
                                  ),),

                              ),

                              // Text("View all",
                              //   style: TextStyle(
                              //       shadows: [
                              //         Shadow(
                              //             color: Colors.blue[900],
                              //             offset: Offset(0, -5))
                              //       ],
                              //       color: Colors.transparent,
                              //       decoration: TextDecoration.underline,
                              //       decorationColor: Colors.indigo,
                              //       decorationThickness: 2,
                              //       //  color:Colors.indigo,
                              //       fontSize:16,
                              //       fontWeight: FontWeight.bold
                              //   ),),

                            ),
                          ],
                        )
                    ),
                    // this section is for the product view
                    Container(
                      // color: Colors.red,
                     height: 500,

                      child: Column(

                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Flexible(
                            child: FutureBuilder(
                              future: _future,
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  print("Snapshot has data");
                                  HomeResponse model = snapshot.data;
                                  //imagedata=widget.categoryResponse.result[1].image;
                                  // Map<String, String> bodyData = snapshot.data;
                                  return GridView.builder(
                                      shrinkWrap: true,
                                      itemCount: model.result.products.length,
                                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 3,
                                          childAspectRatio: 3/3,
                                          crossAxisSpacing: 4,
                                          mainAxisSpacing: 5
                                      ),
                                      itemBuilder: (ctx, index) {
                                        return Container(
                                            color: Colors.grey[300],
                                            child: Card(
                                              child: InkWell(
                                                onTap: (){
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context)=>product_detail(id=model.result.products[index].id),
                                                      )
                                                  );
                                                },
                                                child: Container(
                                                  child: Column(
                                                    children: [
                                                      Container(
                                                        //color: Colors.green,
                                                        alignment: Alignment.center,
                                                        padding:  EdgeInsets.only(top:  15,right: 15,left: 15),
                                                        child: Image.network('http://139.59.75.40:4040/uploads/product/${model.result.products[index].image}',fit: BoxFit.fill,),

                                                      ),
                                                      Container(
                                                        margin: const EdgeInsets.only(top: 18),
                                                        padding: const EdgeInsets.only(left: 5,right: 5),
                                                        width: 140,
                                                        child: Text(model.result.products[index].name,
                                                          style: TextStyle(
                                                              fontSize: 11,
                                                              fontWeight: FontWeight.bold

                                                          ),) ,
                                                      ),
                                                      Container(
                                                          margin: const EdgeInsets.only(top: 4,bottom: 1),
                                                          padding: const EdgeInsets.only(right: 10,left: 10),
                                                          child:Row(
                                                            crossAxisAlignment: CrossAxisAlignment.end,
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            children: [
                                                              Container(
                                                                child: Text("\u20B9"),
                                                              ),
                                                              Container(

                                                                child: Text(model.result.products[index].sellingPrice,style: TextStyle(
                                                                    color: Colors.indigo[800]
                                                                ),),
                                                              ),
                                                              Container(
                                                                width: 15,
                                                                margin: const EdgeInsets.only(left: 40),
                                                                child: Image.asset('assets/love.png',fit: BoxFit.contain),

                                                              ),
                                                            ],
                                                          )
                                                      )
                                                    ],
                                                  ),
                                                ),

                                              )

                                            ),
                                          );
                                      });
                                }
                                else{
                                  print("ERROR.${snapshot.error}");
                                  return Center(
                                      child: Column(
                                        children: [
                                           CircularProgressIndicator(),
                                          //Text("Error.${snapshot.error}")

                                        ],
                                      ));
                                }
                                return CircularProgressIndicator();
                              },
                            ),

                          ),
                        ],
                      ),
                    ),
                    // Container(
                    //   // height: 300,
                    //   // color: Colors.black,
                    //   child: Text("hello ravan"),
                    // ),
                  ],

                ),
              );


            }
            else{
              return Center(
                  child: Container(
                    margin: const EdgeInsets.only(top:300),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                         CircularProgressIndicator(
                         ),
                        Text("Please wait or cheak your Internet connection")

                      ],
                    ),
                  ));
            }

          },
        ),
      ),
    );
  }
}



