

import 'package:blu_customer/Models/cartResponse.dart';
import 'package:blu_customer/Models/productDeatailResponse.dart';
import 'package:blu_customer/Network/api_call.dart';
import 'package:blu_customer/screen/cart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_cart/flutter_cart.dart';
import 'package:provider/provider.dart';



class product_detail extends StatefulWidget {
  final String id;
  product_detail(this.id, {Key key}): super(key: key);

 // final ProductDeatailResponse productDeatailResponse;

 // const product_detail({Key key, this.productDeatailResponse}) : super(key: key);

  @override
  _product_detailState createState() => _product_detailState();
}

class _product_detailState extends State<product_detail> {
  Future<ProductDeatailResponse>_future;

  //var cart = FlutterCart();
  var message;
 String id;
 String productId;
 String name;
 String image;
 double price;
 String size;
 int quantity;
 String _dropDownValue;

  @override
  void initState() {
    id=widget.id;


    print("Hello ravan your id ${id}");
    _future=ApiCall.fetchProductDeatailResponse(id);
    super.initState();
  }


  // addToCart(dynamic _product) => {
  //   message = cart.addToCart(
  //       productId: _product.productId,
  //       unitPrice: _product.productPrice,
  //       quantity: _product.quantity,
  //
  //       uniqueCheck: _product.selectedProductType,
  //
  //       productDetailsObject: _product),
  // };


  circularProgress(){
    return Center(
      child: CircularProgressIndicator(),
    );
  }




  @override
  Widget build(BuildContext context) {
    final cart=Provider.of<Cart>(context);
    return Scaffold(
      appBar:  AppBar(
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.pop(context,true);
            }
        ),
        title: Container(
          padding: const EdgeInsets.only(left: 40),
          child: Text('Product Details',style: TextStyle(
            fontSize: 25,

          ),),
        ),
        backgroundColor:Colors.indigo[800],

        actions: <Widget>[
          // IconButton(
          //   iconSize: 35,
          //   icon: const Icon(Icons.notifications),
          //   tooltip: 'Show Snackbar',
          //   onPressed: () {
          //
          //   },
          //),
        ],
      ),
      body: SingleChildScrollView(
        child: FutureBuilder(
          future: _future,
          builder: (context, snapshot){
             if (snapshot.hasData) {
               print("Snapshot has data");
               ProductDeatailResponse model = snapshot.data;
               return  Container(
                 color: Colors.grey[300],
                 child: Column(
                   // mainAxisAlignment: MainAxisAlignment.spaceAround,
                   children: [
                     Container(
                       margin: const EdgeInsets.only(top: 20),
                       decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(18.0),
                         color: Colors.white,
                       ),

                       padding: const EdgeInsets.only(left: 60,right: 60,top: 30,bottom: 30),

                       child: Image.network('http://139.59.75.40:4040/uploads/product/${model.result.image}',fit: BoxFit.fill,),
                     ),
                     Container(
                       padding: const EdgeInsets.only(top: 20,bottom: 10,left: 20,right: 20),
                       child: Column(
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           Container(

                             child: Text(model.result.name,style: TextStyle(
                                 fontSize: 24,
                                 color: Colors.black,
                                 fontWeight: FontWeight.bold
                             ),),
                             padding: const EdgeInsets.all(5),
                           ),
                           Container(
                             padding: const EdgeInsets.all(5),
                             child: Text(model.result.city,style: TextStyle(
                                 fontSize: 16,
                                 fontWeight: FontWeight.w400
                             ),),

                           ),
                           Row(
                             children: [
                               Container(
                                 child: Text("\u20B9",style:TextStyle(
                                     fontWeight: FontWeight.bold,
                                     fontSize: 24,
                                     color: Colors.indigo
                                 ) ,),
                               ),
                               Container(
                                 padding: const EdgeInsets.all(5),
                                 child: Text(model.result.sellingPrice.toString(),style: TextStyle(
                                     fontWeight: FontWeight.bold,
                                     fontSize: 24,
                                     color: Colors.indigo
                                 ),),
                               ),
                             ],
                           ),
                           Container(
                             padding: const EdgeInsets.all(5),
                             child: Text('Description',style: TextStyle(
                                 fontSize: 20,
                                 fontWeight: FontWeight.w500,
                                 color: Colors.black
                             ),),
                           ),
                           SingleChildScrollView(
                             child: Container(
                               height: 150,

                               padding: const EdgeInsets.all(7),
                               child: Text('A woderful serenity has taken hxgxjjxjhx gzxhnxzfg ghfhgfdghnh'),
                             ),
                           ),
                         ],
                       ),

                     ),
                     // Container(
                     //   padding: const EdgeInsets.all(7),
                     //   alignment: Alignment.center,
                     //   width: 500,
                     //   decoration: BoxDecoration(
                     //       border: Border(
                     //         top: BorderSide(width: 1.0,color: Colors.grey,),
                     //         bottom: BorderSide(width: 1.0,color: Colors.grey,),
                     //       )
                     //   ),
                     //   child: Text('Pack Sizes',style: TextStyle(
                     //       fontWeight: FontWeight.bold,
                     //       fontSize: 22,
                     //       color: Colors.black
                     //   ),),
                     // ),
                     // Container(
                     //     padding: const EdgeInsets.all(10),
                     //     child: Row(
                     //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                     //       children: [
                     //         Container(
                     //           width: 70,
                     //           color:Colors.green,
                     //           alignment: Alignment.center,
                     //           child: Column(
                     //             mainAxisAlignment: MainAxisAlignment.spaceAround,
                     //             children: [
                     //               Container(
                     //                 padding: const EdgeInsets.only(top: 10,left: 5,right: 5,bottom: 2),
                     //
                     //                 child: Text('07',style: TextStyle(
                     //                     fontSize: 25,
                     //                     fontWeight: FontWeight.bold,
                     //                     color: Colors.white
                     //                 ),),
                     //               ),
                     //               Container(
                     //                 padding: const EdgeInsets.all(8),
                     //
                     //                 child: Text('Pads',style: TextStyle(
                     //                     fontSize: 15,
                     //                     fontWeight: FontWeight.bold,
                     //                     color: Colors.white
                     //                 ),),
                     //               ),
                     //
                     //             ],
                     //           ),
                     //         ),
                     //         Container(
                     //           width: 70,
                     //           color:Colors.white,
                     //           alignment: Alignment.center,
                     //           child: Column(
                     //             mainAxisAlignment: MainAxisAlignment.spaceAround,
                     //             children: [
                     //               Container(
                     //                 padding: const EdgeInsets.only(top: 10,left: 5,right: 5,bottom: 2),
                     //
                     //                 child: Text('07',style: TextStyle(
                     //                     fontSize: 25,
                     //                     fontWeight: FontWeight.bold,
                     //                     color: Colors.black
                     //                 ),),
                     //               ),
                     //               Container(
                     //                 padding: const EdgeInsets.all(8),
                     //
                     //                 child: Text('Pads',style: TextStyle(
                     //                     fontSize: 15,
                     //                     fontWeight: FontWeight.bold,
                     //                     color: Colors.black
                     //                 ),),
                     //               ),
                     //
                     //             ],
                     //           ),
                     //         ),
                     //         Container(
                     //           width: 70,
                     //           color:Colors.white,
                     //           alignment: Alignment.center,
                     //           child: Column(
                     //             mainAxisAlignment: MainAxisAlignment.spaceAround,
                     //             children: [
                     //               Container(
                     //                 padding: const EdgeInsets.only(top: 10,left: 5,right: 5,bottom: 2),
                     //
                     //                 child: Text('17',style: TextStyle(
                     //                     fontSize: 25,
                     //                     fontWeight: FontWeight.bold,
                     //                     color: Colors.black
                     //                 ),),
                     //               ),
                     //               Container(
                     //                 padding: const EdgeInsets.all(8),
                     //
                     //                 child: Text('Pads',style: TextStyle(
                     //                     fontSize: 15,
                     //                     fontWeight: FontWeight.bold,
                     //                     color: Colors.black
                     //                 ),),
                     //               ),
                     //
                     //             ],
                     //           ),
                     //         ),
                     //         Container(
                     //           width: 70,
                     //           color:Colors.white,
                     //           alignment: Alignment.center,
                     //           child: Column(
                     //             mainAxisAlignment: MainAxisAlignment.spaceAround,
                     //             children: [
                     //               Container(
                     //                 padding: const EdgeInsets.only(top: 10,left: 5,right: 5,bottom: 2),
                     //
                     //                 child: Text('27',style: TextStyle(
                     //                     fontSize: 25,
                     //                     fontWeight: FontWeight.bold,
                     //                     color: Colors.black
                     //                 ),),
                     //               ),
                     //               Container(
                     //                 padding: const EdgeInsets.all(8),
                     //
                     //                 child: Text('Pads',style: TextStyle(
                     //                     fontSize: 15,
                     //                     fontWeight: FontWeight.bold,
                     //                     color: Colors.black
                     //                 ),),
                     //               ),
                     //
                     //             ],
                     //           ),
                     //         ),
                     //       ],
                     //     )
                     // ),
                     SingleChildScrollView(

                       child: Container(
                         margin: const EdgeInsets.only(top: 165),
                         color:Colors.indigo,
                         child: Row(
                           children: [
                             Container(
                               alignment: Alignment.center,
                               padding: const EdgeInsets.only(top: 10,bottom: 10,left: 10,right: 10),
                               color:Colors.black,
                               child: Text('Qty : ',style: TextStyle(
                                   fontSize: 25,
                                   fontWeight: FontWeight.w300,
                                   color: Colors.white
                               ),),

                             ),
                             Container(
                              padding: const EdgeInsets.only(bottom: 1),
                               color:Colors.black,
                               width: 60,
                               child: new DropdownButtonHideUnderline(
                                 child: DropdownButton(


                                   hint: _dropDownValue == null ?Text('0',style: TextStyle(color: Colors.white,fontSize: 25),)
                                       :Text(_dropDownValue,style: TextStyle(color: Colors.white,fontSize: 25),
                                 ),
                                  // isExpanded: true,
                                   iconSize: 25.0,
                                   style: TextStyle(color: Colors.black),

                                   items: ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17",
                                       "18","19","20","21","22","23","24","25","26","27","28","29","30",

                                   ].map(
                                         (val) {
                                       return DropdownMenuItem<String>(
                                         value: val,
                                         child: Text(val),
                                       );
                                     },

                                   ).toList(),
                                   onChanged: (val) {
                                     setState(
                                           () {
                                         _dropDownValue = val;
                                       },
                                     );
                                   },
                                 ),
                               ),
                             ),
                             Container(
                               alignment: Alignment.center,
                               padding:  EdgeInsets.only(left: 40),
                               color:Colors.indigo,
                               child: InkWell(
                                 onTap: (){
                                   Scaffold.of(context).showSnackBar(SnackBar(
                                     duration: Duration(seconds: 5),
                                     content: Text("item Added to cart"),
                                   ));
                                   // addToCart();
                                   cart.addItem(
                                       productId=model.result.id,
                                       name= model.result.name,
                                       price =double.parse(model.result.sellingPrice),
                                       image=model.result.image,
                                       quantity=model.result.qty,


                                   );
                                 },
                                 child: Text('ADD TO CART',style: TextStyle(
                                     fontSize: 25,
                                     fontWeight: FontWeight.w500,
                                     color: Colors.white
                                 ),),

                               )


                             )
                           ],
                         ),
                       ),
                     ),

                   ],
                 ),
               );
             }
             else{
               return Center(
                   child: Container(
                     margin: const EdgeInsets.only(top:300),
                     child: Column(
                       mainAxisAlignment: MainAxisAlignment.center,
                       crossAxisAlignment: CrossAxisAlignment.center,
                       children: [
                         CircularProgressIndicator(
                         ),
                         Text("Please wait or cheak your Internet connection")

                       ],
                     ),
                   ));
             }
          },
        ),
      ),
    );
  }
}
