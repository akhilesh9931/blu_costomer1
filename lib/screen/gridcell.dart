import 'package:flutter/material.dart';
import 'package:blu_customer/Models/categoryResponse.dart';

class gridcell extends StatelessWidget {
  const gridcell(this.categoryResponse);
  @required
  final CategoryResponse categoryResponse;




  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(2)
      ),
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Container(
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [

              Flexible(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Hero(
                    tag: "image${categoryResponse.status}",
                     child:
                    FadeInImage.assetNetwork(
                      //image:
                      width: 100,
                      height: 100,
                    ),

                  ),


                ),
              )
            ],

          ),
        ),
      ),




      );

  }
}
//image: NetworkImage(categoryResponse.result[1].image),

//child: Container(
//         child: Column(
//           children: [
//             Container(
//               //color: Colors.green,
//               alignment: Alignment.center,
//               // margin: const EdgeInsets.only(top:5,right: 10,left: 10),
//               padding: const EdgeInsets.only(top: 13,right: 13,left: 15,bottom: 1),
//               // child:Image.asset('assets/banner.png',fit: BoxFit.fill),
//
//               decoration: BoxDecoration(
//                 //color: Colors.red,
//                 image: DecorationImage(
//                   //image: AssetImage('assets/banner.png'),
//                   image: NetworkImage(categoryResponse.result[1].image),
//                   fit: BoxFit.contain,
//                 ),
//               ) ,
//             ),
//             Container(
//               margin: const EdgeInsets.only(top: 15),
//               padding: const EdgeInsets.only(left: 5,right: 5),
//               //width: 120,
//               child: Text(categoryResponse.result[0].name,
//                 style: TextStyle(
//                     fontSize: 11,
//                     fontWeight: FontWeight.bold
//
//                 ),) ,
//             ),
//             Container(
//                 margin: const EdgeInsets.only(top: 6,bottom: 1),
//                 padding: const EdgeInsets.only(right: 10,left: 10),
//                 child:Row(
//                   crossAxisAlignment: CrossAxisAlignment.end,
//                   mainAxisAlignment: MainAxisAlignment.start,
//                   children: [
//                     Container(
//                       child: Text("\u20B9"),
//                     ),
//                     Container(
//
//                       child: Text("1299",style: TextStyle(
//                           color: Colors.indigo[800]
//                       ),),
//                     ),
//                     Container(
//                       width: 15,
//                       margin: const EdgeInsets.only(left: 40),
//                       child: Image.asset('assets/love.png',fit: BoxFit.contain),
//
//                     ),
//                   ],
//                 )
//             )
//           ],
//         ),
//       ),