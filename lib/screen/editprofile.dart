import 'package:blu_customer/Models/editProfileResponse.dart';
import 'package:blu_customer/Network/api_call.dart';
import 'package:blu_customer/screen/BottomNavigationBar.dart';
import 'package:blu_customer/screen/profile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';


class editprofile extends StatefulWidget {


  @override
  _editprofileState createState() => _editprofileState();
}

class _editprofileState extends State<editprofile> {

 String email;
 String fullName;
 String phoneNo;
 String address;
 String pinCode;
 String state;
 String city;

  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController fullNameController = TextEditingController();
  final TextEditingController addressController = TextEditingController();
  final TextEditingController pinCodeController = TextEditingController();
  final TextEditingController cityController = TextEditingController();
  final TextEditingController stateController = TextEditingController();

  getDataFromSharedPrefs() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      id = sharedPreferences.getString("tokenCall");
      print(id);
      // firstName = sharedPreferences.getString("firstName");
    });
  }
  final _form = GlobalKey<FormState>();//for storing form state.
  EditProfileResponse _user;
//saving form after validation
  Future<void> _saveForm() async {
    fullName =fullNameController.text;
    phoneNo=phoneNumberController.text;
    email=emailController.text;
    address=addressController.text;
    state=stateController.text;
    city=cityController.text;
    final isValid = _form.currentState.validate();
    if (!isValid) {
      return;
    }
    else{
     final EditProfileResponse editProfileResponse = await ApiCall.postEditProfile(
          id: id,
          fullname: fullName,
          mobile: phoneNo,
          email: email,
          address: address,
          pincode: pinCode,
          city: city,
          state: state,
      );

      if (editProfileResponse == null) {
        print('data not  sent or error');
      }
      else{
        if (editProfileResponse.status == "success") {
          print(editProfileResponse.status);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => profile()),
          );
          setState(() {
            isLoading = false;
            Messstatus = editProfileResponse.message
                .toString();
            print("welcvome to upafte profgile ravan${Messstatus}");
            response =
                editProfileResponse.status;
          });
        }

      }
    }
  }

  String response;
  String id;
  bool isLoading;
  String Messstatus;
  String FullName;


  @override
  void initState() {
    getDataFromSharedPrefs();

    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: new IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MyNavigationBar()),
              );
            }
        ),
        backgroundColor:Colors.indigo[800],
         title: Container(
             padding: const EdgeInsets.only(top: 10,bottom: 5),
             child: Text('Edit Profile', style: TextStyle(fontSize: 28.0,fontWeight: FontWeight.bold),)
         ),

      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Form(
            key: _form,
            child: Column(
              children: [
                Container(
                  height: 200,
                  color: Colors.indigo[800],
                  child: Column(
                    children: [
                      Container(
                          //padding: const EdgeInsets.only(bottom: 20),
                         margin: const EdgeInsets.only(top: 10,bottom: 10),
                          height: 125,
                         decoration: BoxDecoration(
                             //borderRadius: BorderRadius.circular(50),
                           shape: BoxShape.circle,
                             color: Colors.redAccent,
                            image: DecorationImage(
                             image:AssetImage('assets/banner.png'),
                             // image: NetworkImage("ADD IMAGE URL HERE"),
                             fit: BoxFit.fill,
                           ),
                          ),
                      ),
                      // Container(
                      //   child: Text("Jhon smit",style: TextStyle(
                      //     fontSize: 25,
                      //     fontWeight: FontWeight.bold,
                      //     color: Colors.white
                      //   ),),
                      // ),

                    ],
                  ),
                ),
//this is for name and last name
                Container(
                  child: Row(
                    children: [
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                             padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                              child: Text("Full name",style: TextStyle(
                                color: Colors.grey,
                                fontSize: 18
                              ),),
                            ),
                            Container(
                              //color: Colors.redAccent,
                               height: 35,
                               width: 350,
                               padding: const EdgeInsets.only(left: 15,right: 10,bottom: 5),
                              child: TextFormField(
                                controller: fullNameController,
                                autocorrect: true,
                                decoration: InputDecoration(
                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      width: 2,
                                      color: Colors.grey
                                    ),
                                  ),
                                  focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color:Colors.black)
                                  )
                                ),

                              ),
                            ),
                          ],
                        ),
                      ),
                      // Container(
                      //   margin: const EdgeInsets.only(left: 10),
                      //   child: Column(
                      //     mainAxisAlignment: MainAxisAlignment.start,
                      //     crossAxisAlignment: CrossAxisAlignment.start,
                      //     children: [
                      //       Container(
                      //         padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                      //         child: Text("Last name",style: TextStyle(
                      //             color: Colors.grey,
                      //             fontSize: 18
                      //         ),),
                      //       ),
                      //       Container(
                      //         //color: Colors.redAccent,
                      //         height: 35,
                      //         width: 180,
                      //         padding: const EdgeInsets.only(left: 15,right: 10,bottom: 5),
                      //         child: TextField(
                      //           autocorrect: true,
                      //           decoration: InputDecoration(
                      //               enabledBorder: UnderlineInputBorder(
                      //                 borderSide: BorderSide(
                      //                     width: 2,
                      //                     color: Colors.grey
                      //                 ),
                      //               ),
                      //               focusedBorder: UnderlineInputBorder(
                      //                   borderSide: BorderSide(color:Colors.black)
                      //               )
                      //           ),
                      //
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                    ],
                  ),
                ),

                //this is for the email

                Container(
                  // margin: const EdgeInsets.only(top: 10,) ,
                  alignment: Alignment.bottomLeft,
                  padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                  child: Text("Email",style: TextStyle(
                      color: Colors.grey,
                      fontSize: 18
                  ),),
                ),
                Container(
                  alignment: Alignment.bottomLeft,
                  //color: Colors.redAccent,
                  height: 35,
                  width: 340,
                  padding: const EdgeInsets.only(right: 10,bottom: 5),
                  child: TextFormField(
                    controller: emailController,
                    autocorrect: true,
                    decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                              width: 2,
                              color: Colors.grey
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color:Colors.black)
                        )
                    ),

                  ),
                ),

                //this is for address
                Container(
                 // margin: const EdgeInsets.only(top: 10,) ,
                  alignment: Alignment.bottomLeft,
                  padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                  child: Text("Address",style: TextStyle(
                      color: Colors.grey,
                      fontSize: 18
                  ),),
                ),
                Container(
                  alignment: Alignment.bottomLeft,
                  //color: Colors.redAccent,
                  height: 35,
                  width: 340,
                  padding: const EdgeInsets.only(right: 10,bottom: 5),
                  child: TextFormField(
                    controller: addressController,
                    autocorrect: true,
                    decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                              width: 2,
                              color: Colors.grey
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color:Colors.black)
                        )
                    ),

                  ),
                ),
                //this is for phone number and zip code
                Container(
                  child: Row(
                    children: [
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                              child: Text("Phone number",style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 18
                              ),),
                            ),
                            Container(
                              //color: Colors.redAccent,
                              height: 35,
                              width: 180,
                              padding: const EdgeInsets.only(left: 15,right: 10,bottom: 5),
                              child: TextFormField(
                                controller: phoneNumberController,
                                autocorrect: true,
                                decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 2,
                                          color: Colors.grey
                                      ),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color:Colors.black)
                                    )
                                ),

                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                              child: Text("Zip Code",style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 18
                              ),),
                            ),
                            Container(
                              //color: Colors.redAccent,
                              height: 35,
                              width: 150,
                              padding: const EdgeInsets.only(left: 15,right: 10,bottom: 5),
                              child: TextFormField(
                                controller: pinCodeController,
                                autocorrect: true,
                                decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 2,
                                          color: Colors.grey
                                      ),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color:Colors.black)
                                    )
                                ),

                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    children: [
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                              child: Text("City",style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 18
                              ),),
                            ),
                            Container(
                              //color: Colors.redAccent,
                              height: 35,
                              width: 180,
                              padding: const EdgeInsets.only(left: 15,right: 10,bottom: 5),
                              child: TextFormField(
                                controller: cityController,
                                autocorrect: true,
                                decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 2,
                                          color: Colors.grey
                                      ),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color:Colors.black)
                                    )
                                ),

                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: const EdgeInsets.only(left: 15,top: 10,right: 15,) ,
                              child: Text("State",style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 18
                              ),),
                            ),
                            Container(
                              //color: Colors.redAccent,
                              height: 35,
                              width: 150,
                              padding: const EdgeInsets.only(left: 15,right: 10,bottom: 5),
                              child: TextFormField(
                                controller: stateController,
                                autocorrect: true,
                                decoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 2,
                                          color: Colors.grey
                                      ),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color:Colors.black)
                                    )
                                ),

                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),

                //this is for the save button

                Container(
                  margin: const EdgeInsets.only(top: 30),
                  padding: const EdgeInsets.only(top: 10),
                  width: 300,
                  height: 60,
                  //color: Colors.red,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        side: BorderSide(color: Colors.black)),
                    onPressed: () async{
                      print('update');
                      final EditProfileResponse editProfileResponse = await ApiCall.postEditProfile(
                        id: id,
                        fullname: fullName,
                        mobile: phoneNo,
                        email: email,
                        address: address,
                        pincode: pinCode,
                        city: city,
                        state: state,
                      );
                      if (editProfileResponse.status == "success") {
                        print(editProfileResponse);
                        var response = editProfileResponse;
                        setState(() {
                          fullName =fullNameController.text;
                          phoneNo=phoneNumberController.text;
                          email=emailController.text;
                          address=addressController.text;
                          state=stateController.text;
                          city=cityController.text;
                        });
                        print(editProfileResponse.status);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => profile()),
                        );
                        setState(() {
                          isLoading = false;
                          Messstatus = editProfileResponse.message
                              .toString();
                          print("welcvome to upafte profgile ravan${Messstatus}");

                        });
                      }

                    },
                    //=> _saveForm(),

                    color: Colors.indigo[800],
                    textColor: Colors.black,
                    child: Text("Save",
                      style: TextStyle(
                          fontSize: 28,
                          color: Colors.white,
                          fontWeight: FontWeight.bold
                      ),),

                  ),

                ),
              ],

            ),
          ),
        ),
      ),
    );
  }
}
