import 'package:blu_customer/screen/BottomNavigationBar.dart';
import 'package:flutter/material.dart';


class orderSuccess extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Container(
                height: 500,
                child: Image.asset("assets/thank-you.png",
                  fit: BoxFit.contain,
                  //height: 142,
                ),

              ),
              Container(
                child: Text('Confirmation',style: TextStyle(
                  fontSize: 40,
                  color: Colors.black
                ),),
              ),
              Container(
                margin: const EdgeInsets.only(top: 20),

                alignment: Alignment.center,
                width: 250,
                child: Text('You have Sucessfully ',style: TextStyle(
                  fontSize: 17,
                    color: Colors.black54
                ),),
              ),
              Container(
                margin: const EdgeInsets.only(top:2),

                alignment: Alignment.center,
                width: 330,
                child: Text('completed your payment procedure',style: TextStyle(
                    fontSize: 17,
                    color: Colors.black54
                ),),
              ),
              Container(
                margin: const EdgeInsets.only(top: 160),
               // width: 260,
               alignment: Alignment.center,
                padding: const EdgeInsets.only(top: 10,bottom: 10,left: 50,right: 50,),
                color: Colors.indigo,
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MyNavigationBar()),
                    );

                  },
                  child: Text('BACK TO HOME',style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.bold,
                    color: Colors.white
                  ),),

                ),
              ),
            ],
          ),

        ),
      ),
    );
  }
}
